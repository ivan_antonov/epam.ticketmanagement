﻿Feature: TicketManagementAddEvent
		As a EventManager
		I want to be able to create an event
		So I can do it event's event create page

	Scenario: Create an event is succeed
		Given Authorized User is on http://localhost:62201/ with username "admin" password "adminadmin"
		When User clicks event's manager 
		And Enters "SomeName" to event name input
		And Enters "Something description" to event description input
		And Enters "/Content/images/test.jpeg" to event poster input
		And Enters "22/12/2018" to event date start input
		And Enters "10:00" to event time start input
		And Enters "22/12/2018" to event date end input
		And Enters "12:00" to event time end input
		And Clicks venue:1 button
		And Clicks layout:1 button
		And Clicks add event button
		Then Event with id:5 exist
	
	Scenario: Create an event is failed  with date in past
		Given Authorized User is on http://localhost:62201/ with username "admin" password "adminadmin"
		When User clicks event's manager 
		And Enters "SomeName" to event name input
		And Enters "Something description" to event description input
		And Enters "/Content/images/test.jpeg" to event poster input
		And Enters "22/12/2016" to event date start input
		And Enters "10:00" to event time start input
		And Enters "22/12/2016" to event date end input
		And Enters "12:00" to event time end input
		And Clicks venue:1 button
		And Clicks layout:1 button
		And Clicks add event button
		Then Error banner has error text "Проверьте правильность дат. Выберите расположение"