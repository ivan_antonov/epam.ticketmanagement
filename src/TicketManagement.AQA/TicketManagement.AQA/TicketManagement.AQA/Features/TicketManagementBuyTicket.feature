﻿Feature: TicketManagementBuyTicket
		As a User 
		I want to be able to buy a ticket
		So I can do it event's detail page

	Scenario: Buy a ticket is succeed
		Given Authorized User is on http://localhost:62201/ with username "anyuser" password "useruser"
		When User clicks event "3" with purchased seat
		And Clicks on the seat:42
		And Clicks button buy 
		Then Info banner has text "You bought seats"

	Scenario: Buy a ticket is failed with not enought money on balance
		Given Authorized User is on http://localhost:62201/ with username "anyuser" password "useruser"
		When User clicks event "1" with purchased seat
		And Clicks on the seat:1
		And Clicks button buy 
		Then Info banner has error text "Insufficient funds on your balance"