﻿Feature: TicketManagementUpdateEvent
		As a EventManager
		I want to be able to update an event
		So I can do it event's event update page

	Scenario: Update an event is succeed
		Given Authorized User is on http://localhost:62201/ with username "admin" password "adminadmin"
		When User clicks event "1" with purchased seat
		And Clicks edit event button
		And Enters "Edit" to event name input
		And Enters "Something description" to event description input
		And Clicks edit event button on form edit event
		Then Event with id:1 has name "SnowmanEdit"
	
	Scenario: Update an event is failed  with date in past
		Given Authorized User is on http://localhost:62201/ with username "admin" password "adminadmin"
		When User clicks event "1" with purchased seat
		And Clicks edit event button
		And Enters "SomeNameEdit" to event name input
		And Enters "22/12/2016" to event date start input
		And Enters "22/12/2016" to event date end input
		And Clicks edit event button on form edit event
		Then Error banner has error text "Проверьте правильность дат. Проверьте ввденные данные"