﻿Feature: TicketManagementLogin
	As a user 
	I want to be able to login site
	So I can do it from login form

	Scenario: Login to http://localhost:62201/ is failed with wrong credentials
		Given Unauthorized user is on http://localhost:62201/
		When User clicks Login button
		And Enters "user17" to user name input
		And Enters "123456789Qq" to password field
		And Clicks Sign In button on login form
		Then Login form has error "Пользователь с таким именем или паролем не существут"

	Scenario: Login to http://localhost:62201/ is succeed
		Given Unauthorized user is on http://localhost:62201/
		When User clicks Login button
		And Enters "admin" to user name input
		And Enters "adminadmin" to password field
		And Clicks Sign In button on login form
		Then Label text is "admin"

	Scenario: Login to http://localhost:62201/ is succeed for User
		Given Unauthorized user is on http://localhost:62201/
		When User clicks Login button
		And Enters "anyuser" to user name input
		And Enters "useruser" to password field
		And Clicks Sign In button on login form
		And User is on http://localhost:62201/User
		Then Label UserRole text is "User"

	Scenario: Login to http://localhost:62201/ is succeed for VenueManager
		Given Unauthorized user is on http://localhost:62201/
		When User clicks Login button
		And Enters "admin" to user name input
		And Enters "adminadmin" to password field
		And Clicks Sign In button on login form
		And User is on http://localhost:62201/User
		Then Label VenueManagerRole text is "VenueManager"

	Scenario: Login to http://localhost:62201/ is succeed for EventManager
		Given Unauthorized user is on http://localhost:62201/
		When User clicks Login button
		And Enters "admin" to user name input
		And Enters "adminadmin" to password field
		And Clicks Sign In button on login form
		And User is on http://localhost:62201/User
		Then Label EventManagerRole text is "EventManager"