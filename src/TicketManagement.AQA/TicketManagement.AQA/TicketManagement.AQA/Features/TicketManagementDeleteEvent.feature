﻿Feature: TicketManagementDeleteEvent
		As a EventManager 
		I want to be able to delete event
		So I can do it event's detail page

	Scenario: Delete event is failed with purchased seats
		Given Authorized User is on http://localhost:62201/ with username "admin" password "adminadmin"
		When User clicks event "1" with purchased seat
		And Clicks Delete event button
		Then Error banner has error "Событие не может быть удалено. Кто-то уже купил билет."

	Scenario: Delete event is succed
		Given Authorized User is on http://localhost:62201/ with username "admin" password "adminadmin"
		When User clicks event "4" with purchased seat
		And Clicks Delete event button
		Then Event with id:4 not found in the system