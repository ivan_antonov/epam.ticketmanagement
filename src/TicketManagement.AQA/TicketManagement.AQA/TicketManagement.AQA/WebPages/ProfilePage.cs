﻿using OpenQA.Selenium;
using TicketManagement.AQA.Utils;

namespace TicketManagement.AQA.WebPages
{
    public class ProfilePage : AbstractPage
    {
        public ProfilePage(IWebDriver driver)
            : base(driver) { }

        public string UserRoleLabel => FindByCss("#UserRole").Text;
        public string VenueManagerRoleLabel => FindByCss("#VenueManagerRole").Text;
        public string EventManagerRoleLabel => FindByCss("#EventManagerRole").Text;

        public void Open()
        {
            Driver.Url = TestConfiguration.ApplicationUrl + "/User";
        }
    }
}
