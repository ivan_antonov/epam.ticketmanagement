﻿using OpenQA.Selenium;

namespace TicketManagement.AQA.WebPages
{
    public class NotFoundPage : AbstractPage
    {
        public NotFoundPage(IWebDriver driver)
            : base(driver) { }

        public string CodeText => FindByCss("#code").Text;
    }
}
