﻿using OpenQA.Selenium;
using TicketManagement.AQA.Utils;

namespace TicketManagement.AQA.WebPages
{
    public class EventHomePage : AbstractPage
    {
        public EventHomePage(IWebDriver driver)
            : base(driver) { }


        public void Open()
        {
            Driver.Url = TestConfiguration.ApplicationUrl;
        }

        public IWebElement GetElementEventById(int id)
        {
            return FindByCss($"#event-{id}");
        }
    }
}
