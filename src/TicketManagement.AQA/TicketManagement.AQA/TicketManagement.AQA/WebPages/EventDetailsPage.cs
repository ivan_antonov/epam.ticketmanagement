﻿using OpenQA.Selenium;
using TicketManagement.AQA.Utils;

namespace TicketManagement.AQA.WebPages
{
    public class EventDetailsPage : AbstractPage
    {
        public EventDetailsPage(IWebDriver driver)
            : base(driver) { }

        public IWebElement EditButton => FindByCss("#EditButton");
        public IWebElement DeleteButton => FindByCss("#DeleteButton");
        public IWebElement BuySeatButton => FindByCss("#BuySeat");
        public string ErrorBanner => FindByCss("#errorText").Text;
        public string SucceedBanner => FindByCss("#succeedText").Text;

        public void Open(int id)
        {
            Driver.Url = TestConfiguration.ApplicationUrl+"/Event/Details/" + id;
        }

        public IWebElement GetElementSeatById(int id)
        {
            return FindByCss($"#checkbox_{id}");
        }
    }
}
