﻿using OpenQA.Selenium;
using TicketManagement.AQA.Steps;

namespace TicketManagement.AQA.WebPages
{
    public class LoginPage : AbstractPage
    {
        public LoginPage(IWebDriver driver)
            : base(driver) { }

        private static NavigationPartialPage NavigationPartialPage => PageFactory.Instance.Get<NavigationPartialPage>();

        public IWebElement UserNameInput => FindByCss("#UserName");
        public IWebElement PasswordInput => FindByCss("#Password");
        public IWebElement LanguageRuButton => FindByCss("#lang-ru");
        public IWebElement SubmitLoginFormButton => FindByCss("button[type='submit']");
        public string LoginFormError => FindByCss("#error-text").Text;

        public void Authorize(string userName, string password)
        {
            if (!PageFactory.IsElementPresent("#loginLink"))
                FindByCss("#logoutLink").Click();
            FindByCss("#loginLink").Click();
            UserNameInput.SendKeys(userName);
            PasswordInput.SendKeys(password);
            SubmitLoginFormButton.Submit();
        }
    }
}