﻿using OpenQA.Selenium;
using TicketManagement.AQA.Utils;

namespace TicketManagement.AQA.WebPages
{
    public class EventEditPage : AbstractPage
    {
        public EventEditPage(IWebDriver driver)
            : base(driver) { }

        public IWebElement NameInput => FindByCss("#Name");
        public IWebElement DescriptionInput => FindByCss("#Description");
        public IWebElement PosterInput => FindByCss("#posterId");
        public IWebElement DateStartInput => FindByCss("#DateStart");
        public IWebElement TimeStartInput => FindByCss("#TimeStart");
        public IWebElement DateEndInput => FindByCss("#DateEnd");
        public IWebElement TimeEndInput => FindByCss("#TimeEnd");
        public IWebElement AddEventButton => FindByCss("#AddEvent");
        public IWebElement EditEventButton => FindByCss("#EditEvent");
        public string ErrorBanner => FindByCss("#errorText").Text;

        public void Open()
        {
            Driver.Url = TestConfiguration.ApplicationUrl + "/Manager";
        }

        public IWebElement GetVenueById(int id)
        {
            return GetElementById("venue", id);
        }
        public IWebElement GetLayoutById(int id)
        {
            return GetElementById("layoutName", id);
        }

        private IWebElement GetElementById(string prefix, int id)
        {
            return FindByCss($"#{prefix}_{id}");
        }
    }
}
