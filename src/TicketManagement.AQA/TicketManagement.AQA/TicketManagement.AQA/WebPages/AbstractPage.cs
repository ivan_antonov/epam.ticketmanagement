﻿using OpenQA.Selenium;

namespace TicketManagement.AQA.WebPages
{
    public abstract class AbstractPage
    {
        protected IWebDriver Driver;

        protected AbstractPage(IWebDriver driver)
        {
            Driver = driver;
        }

        protected IWebElement FindByCss(string css) => Driver.FindElement(By.CssSelector(css));

    }
}
