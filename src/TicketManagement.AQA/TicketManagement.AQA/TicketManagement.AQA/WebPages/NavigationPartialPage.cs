﻿using OpenQA.Selenium;

namespace TicketManagement.AQA.WebPages
{
    public class NavigationPartialPage : AbstractPage
    {
        public NavigationPartialPage(IWebDriver driver) 
            : base(driver){}

        public IWebElement LogInButton => FindByCss("#loginLink");
        public IWebElement LogOutButton => FindByCss("#logoutLink");
        public string UserNameLabel => FindByCss("#UserName").Text;
    }
}
