﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;

namespace TicketManagement.AQA.Utils
{
    public class DriverFactory
    {
        public static IWebDriver GetDriver()
        {
            IWebDriver driver;
            switch (TestConfiguration.Browser)
            {

                case "chrome":
                    driver = new ChromeDriver();
                    break;
                case "firefox":
                    driver = new FirefoxDriver();
                    break;
                case "edge":
                    driver = new EdgeDriver();
                    break;
                default: driver = new ChromeDriver(); break;
            }

            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().PageLoad = TimeSpan.
                FromMilliseconds(TestConfiguration.PageLoad);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.
                FromMilliseconds(TestConfiguration.ImplicitWait);
            driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.
                FromMilliseconds(TestConfiguration.AsynchronousJavaScript);
            return driver;
        }
    }
}