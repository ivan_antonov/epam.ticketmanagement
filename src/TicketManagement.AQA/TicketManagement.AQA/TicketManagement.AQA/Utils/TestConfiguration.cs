﻿using System.Configuration;

namespace TicketManagement.AQA.Utils
{
    public class TestConfiguration
    {
        public static string Browser { get; set; }
        public static double PageLoad { get; set; }
        public static double ImplicitWait { get; set; }
        public static double AsynchronousJavaScript { get; set; }
        public static string ApplicationUrl { get; set; }

        static TestConfiguration()
        {
            var reader = new AppSettingsReader();

            Browser = (string)reader.GetValue("Browser", typeof(string));
            PageLoad = (double)reader.GetValue("PageLoad", typeof(double));
            ImplicitWait = (double)reader.GetValue("ImplicitWait", typeof(double));
            AsynchronousJavaScript = (double)reader.GetValue("AsynchronousJavaScript", typeof(double));
            ApplicationUrl = (string)reader.GetValue("ApplicationUrl", typeof(string));
        }
    }
}

