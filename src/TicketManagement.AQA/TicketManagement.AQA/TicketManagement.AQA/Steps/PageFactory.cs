﻿using System;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using TicketManagement.AQA.Utils;
using TicketManagement.AQA.WebPages;

namespace TicketManagement.AQA.Steps
{
    [Binding]
    public class PageFactory
    {
        private static IWebDriver _driver;

        private PageFactory() { }

        private static readonly Lazy<PageFactory> Lazy = new Lazy<PageFactory>(() => new PageFactory());

        public static PageFactory Instance => Lazy.Value;

        public T Get<T>() where T : AbstractPage
        {
            object[] args = { _driver };
            return (T)Activator.CreateInstance(typeof(T), args);
        }

        [BeforeFeature]
        public static void OpenBrowser()
        {
            _driver = DriverFactory.GetDriver();
        }

        [AfterFeature]
        public static void CloseBrowser()
        {
            _driver.Close();
        }

        public static bool IsElementPresent(string css)
        {
            try
            {
                _driver.FindElement(By.CssSelector(css));
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
