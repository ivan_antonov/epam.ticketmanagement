﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TicketManagement.AQA.WebPages;

namespace TicketManagement.AQA.Steps
{
    [Binding]
    public class TicketManagementAddEventSteps
    {
        private static EventEditPage EventEditPage => PageFactory.Instance.Get<EventEditPage>();

        [When(@"User clicks event's manager")]
        public void WhenUserClicksEventSManager()
        {
            EventEditPage.Open();
        }

        [When(@"Enters ""(.*)"" to event name input")]
        public void WhenEntersToEventNameInput(string name)
        {
            EventEditPage.NameInput.SendKeys(name);
        }

        [When(@"Enters ""(.*)"" to event description input")]
        public void WhenEntersToEventDescriptionInput(string description)
        {
            EventEditPage.DescriptionInput.SendKeys(description);
        }

        [When(@"Enters ""(.*)"" to event poster input")]
        public void WhenEntersToEventPosterInput(string posterUrl)
        {
            EventEditPage.PosterInput.SendKeys(posterUrl);
        }

        [When(@"Enters ""(.*)"" to event date start input")]
        public void WhenEntersToEventDateStartInput(string date)
        {
            EventEditPage.DateStartInput.Clear();
            EventEditPage.DateStartInput.SendKeys(date);
        }

        [When(@"Enters ""(.*)"" to event time start input")]
        public void WhenEntersToEventTimeStartInput(string time)
        {            
            EventEditPage.TimeStartInput.SendKeys(time);
        }

        [When(@"Enters ""(.*)"" to event date end input")]
        public void WhenEntersToEventDateEndInput(string date)
        {
            EventEditPage.DateEndInput.Clear();
            EventEditPage.DateEndInput.SendKeys(date);
        }

        [When(@"Enters ""(.*)"" to event time end input")]
        public void WhenEntersToEventTimeEndInput(string time)
        {
            EventEditPage.TimeEndInput.SendKeys(time);
        }

        [When(@"Clicks venue:(.*) button")]
        public void WhenClicksVenueButton(int id)
        {
            EventEditPage.GetVenueById(id).Click();
        }

        [When(@"Clicks layout:(.*) button")]
        public void WhenClicksLayoutButton(int id)
        {
            var elem = EventEditPage.GetLayoutById(id);
            elem.Click();
        }

        [When(@"Clicks add event button")]
        public void WhenClicksAddEventButton()
        {
            EventEditPage.AddEventButton.Click();
        }

        [Then(@"Event with id:(.*) exist")]
        public void ThenEventWithIdExist(int id)
        {
            var isExist = PageFactory.IsElementPresent($"#event-{id}");
            Assert.That(isExist, Is.True);
        }

        [Then(@"Error banner has error text ""(.*)""")]
        public void ThenErrorBannerHaErrorText(string text)
        {
            var actualText = EventEditPage.ErrorBanner.Replace("\r\n", ". ");
            Assert.That(actualText, Is.EqualTo(text));
        }

    }
}
