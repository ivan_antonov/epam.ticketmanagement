﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TicketManagement.AQA.WebPages;

namespace TicketManagement.AQA.Steps
{
    [Binding]
    public class TicketManagementUpdateEventSteps
    {
        private static EventDetailsPage EventDetailsPage => PageFactory.Instance.Get<EventDetailsPage>();
        private static EventEditPage EventEditPage => PageFactory.Instance.Get<EventEditPage>();
        private static EventHomePage EventHomePage => PageFactory.Instance.Get<EventHomePage>();

        [When(@"Clicks edit event button on form edit event")]
        public void WhenClicksEditEventButtonOnFormEditEvent()
        {
            EventEditPage.EditEventButton.Click();
        }


        [When(@"Clicks edit event button")]
        public void WhenClicksEditEventButton()
        {
            EventDetailsPage.EditButton.Click();
        }

        [Then(@"Event with id:(.*) has name ""(.*)""")]
        public void ThenEventWithIdHasName(int id, string name)
        {
            var actualText = EventHomePage.GetElementEventById(id).Text;
            Assert.That(actualText, Is.EqualTo(name));
        }
    }
}
