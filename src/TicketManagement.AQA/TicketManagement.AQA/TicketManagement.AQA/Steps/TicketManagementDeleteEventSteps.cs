﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TicketManagement.AQA.WebPages;

namespace TicketManagement.AQA.Steps
{
    [Binding]
    public class TicketManagementDeleteEventSteps
    {
        private static EventHomePage EventHomePage => PageFactory.Instance.Get<EventHomePage>();
        private static EventDetailsPage EventDetailsPage => PageFactory.Instance.Get<EventDetailsPage>();
        private static LoginPage LoginPage => PageFactory.Instance.Get<LoginPage>();
        private static NotFoundPage NotFoundPage => PageFactory.Instance.Get<NotFoundPage>();

        [Given(@"Authorized User is on http://localhost:62201/ with username ""(.*)"" password ""(.*)""")]
        public void GivenAuthorizedUserIsOnHttpLocalhost(string userName, string password)
        {
            EventHomePage.Open();
            LoginPage.Authorize(userName, password);
        }

        [When(@"User clicks event ""(.*)"" with purchased seat")]
        public void WhenUserClicksEventWithPurchasedSeat(int id)
        {
            EventHomePage.GetElementEventById(id).Click();
        }

        [When(@"Clicks Delete event button")]
        public void WhenClicksDeleteEventButton()
        {
            EventDetailsPage.DeleteButton.Click();
        }

        [Then(@"Error banner has error ""(.*)""")]
        public void ThenErrorBannerHasError(string error)
        {
            var actualText = EventDetailsPage.ErrorBanner;
            Assert.That(actualText, Is.EqualTo(error));
        }

        [Then(@"Event with id:(.*) not found in the system")]
        public void ThenEventWithIdNotFoundInTheSystem(int id)
        {
            var isExist = PageFactory.IsElementPresent($"event-{id}");
            Assert.That(isExist, Is.False);
        }

    }
}
