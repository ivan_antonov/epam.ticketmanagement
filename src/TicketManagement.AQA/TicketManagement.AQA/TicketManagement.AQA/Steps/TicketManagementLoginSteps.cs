﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using TicketManagement.AQA.WebPages;

namespace TicketManagement.AQA.Steps
{
    [Binding]
    public class TicketManagementLoginSteps
    {
        private static NavigationPartialPage NavigationPartialPage => PageFactory.Instance.Get<NavigationPartialPage>();
        private static EventHomePage EventHomePage => PageFactory.Instance.Get<EventHomePage>();
        private static LoginPage LoginPage => PageFactory.Instance.Get<LoginPage>();
        private static ProfilePage ProfilePage => PageFactory.Instance.Get<ProfilePage>();

        [Given(@"Unauthorized user is on http://localhost:62201/")]
        public void GivenUnauthorizedUserIsOnHttpLocalhost()
        {
            EventHomePage.Open();
            if (!PageFactory.IsElementPresent("#loginLink"))
                NavigationPartialPage.LogOutButton.Click();
        }

        [When(@"User clicks Login button")]
        public void WhenUserClicksLoginButton()
        {
            NavigationPartialPage.LogInButton.Click();
            LoginPage.LanguageRuButton.Click();
        }

        [When(@"Enters ""(.*)"" to user name input")]
        public void WhenEntersToUserNameInput(string userName)
        {
            LoginPage.UserNameInput.SendKeys(userName);
        }

        [When(@"Enters ""(.*)"" to password field")]
        public void WhenEntersToPasswordField(string password)
        {
            LoginPage.PasswordInput.SendKeys(password);
        }

        [When(@"Clicks Sign In button on login form")]
        public void WhenClicksSignInButtonOnLoginForm()
        {
            LoginPage.SubmitLoginFormButton.Submit();
        }

        [When(@"User is on http://localhost:62201/User")]
        public void WhenUserIsOnHttpLocalhostUser()
        {
            ProfilePage.Open();
        }

        [Then(@"Login form has error ""(.*)""")]
        public void ThenLoginFormHasError(string expectedErrorMsg)
        {
            var actualText = string.Empty;
            Assert.DoesNotThrow(() => actualText = LoginPage.LoginFormError);
            Assert.That(actualText, Is.EqualTo(expectedErrorMsg));
        }

        [Then(@"Label text is ""(.*)""")]
        public void ThenLabelTextIs(string userName)
        {
            var actualText = NavigationPartialPage.UserNameLabel;
            Assert.That(actualText, Is.EqualTo(userName));
        }

        [Then(@"Label UserRole text is ""(.*)""")]
        public void ThenLabelUserRoleTextIs(string userRole)
        {
            var actualText = ProfilePage.UserRoleLabel;
            Assert.That(actualText, Is.EqualTo(userRole));
        }

        [Then(@"Label VenueManagerRole text is ""(.*)""")]
        public void ThenLabelVenueManagerRoleTextIs(string userRole)
        {
            var actualText = ProfilePage.VenueManagerRoleLabel;
            Assert.That(actualText, Is.EqualTo(userRole));
        }

        [Then(@"Label EventManagerRole text is ""(.*)""")]
        public void ThenLabelEventManagerRoleTextIs(string userRole)
        {
            var actualText = ProfilePage.EventManagerRoleLabel;
            Assert.That(actualText, Is.EqualTo(userRole));
        }
    }
}
