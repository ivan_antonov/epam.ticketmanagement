﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using TicketManagement.AQA.WebPages;

namespace TicketManagement.AQA.Steps
{
    [Binding]
    public class TicketManagementBuyTicketSteps
    {
        private static EventHomePage EventHomePage => PageFactory.Instance.Get<EventHomePage>();
        private static EventDetailsPage EventDetailsPage => PageFactory.Instance.Get<EventDetailsPage>();
        private static LoginPage LoginPage => PageFactory.Instance.Get<LoginPage>();
        private static NotFoundPage NotFoundPage => PageFactory.Instance.Get<NotFoundPage>();

        [When(@"Clicks on the seat:(.*)")]
        public void WhenClicksOnTheSeat(int id)
        {
            EventDetailsPage.GetElementSeatById(id).Click();
        }

        [When(@"Clicks button buy")]
        public void WhenClicksButtonBuy()
        {
            EventDetailsPage.BuySeatButton.Click();
        }

        [Then(@"Info banner has text ""(.*)""")]
        public void ThenInfoBannerHasText(string text)
        {
            var actualText = EventDetailsPage.SucceedBanner;
            Assert.That(actualText, Is.EqualTo(text));
        }

        [Then(@"Info banner has error text ""(.*)""")]
        public void ThenInfoBannerHasErrorText(string text)
        {
            var actualText = EventDetailsPage.ErrorBanner;
            Assert.That(actualText, Is.EqualTo(text));
        }
    }
}
