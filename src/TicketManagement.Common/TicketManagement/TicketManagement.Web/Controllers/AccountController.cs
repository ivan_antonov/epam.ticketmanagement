﻿namespace TicketManagement.Web.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Infrastructure.Authentication;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Models;
    using UserServiceReference;
    using Util;

    public class AccountController : Controller
    {
        private IAuthenticationManager authenticationManager => HttpContext.GetOwinContext().Authentication;

        private readonly UserManager<User, string> userManager;
        private readonly UserWCFServiceClient userService;

        public AccountController(UserManager<User, string> userManager, UserWCFServiceClient userService)
        {
            this.userManager = userManager;
            this.userService = userService;
        }

        public ActionResult Login(string returnUrl)
        {
            var model = new LoginViewModel
            {
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await userManager.FindAsync(model.UserName, model.Password);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, @Resources.Resource.ErrorInvalidData);
                return View(model);
            }

            var claim = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            claim.AddClaim(new Claim("Token", WebClient.GetToken(GlobalSettings.UrlUserApi.Token, model)));

            authenticationManager.SignOut();
            authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claim);

            SetCredentials();

            var culture = userService.GetLanguages().FirstOrDefault(dto => dto.Id == user.LanguageId)?.ShortName ?? "ru";

            var cookie = Request.Cookies["lang"];
            if (cookie != null)
            {
                cookie.Value = culture;
            }
            else
            {
                cookie = new HttpCookie("lang")
                {
                    HttpOnly = false,
                    Value = culture,
                    Expires = DateTime.Now.AddYears(1)
                };
            }

            Response.Cookies.Add(cookie);

            if (model.ReturnUrl != null)
            {
                return Redirect(model.ReturnUrl);
            }

            return RedirectToAction("Index", new { area = string.Empty, controller = "Event" });
        }

        [Authorize]
        public ActionResult Logout()
        {
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Event");
        }

        public ActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = new User
            {
                UserName = model.UserName,
                Email = model.Email,
                FirstName = model.FirstName,
                Surname = model.Surname,
                Balance = 0,
                LanguageId = 1,
                TimeZone = TimeZoneInfo.Local.Id
            };

            var result = await this.userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("Login", "Account", user);
            }

            ViewBag.Error = result.Errors.FirstOrDefault();

            return View();
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var identity = (ClaimsIdentity)this.User.Identity;
            var id = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            var result = await this.userManager.ChangePasswordAsync(id, model.CurrentPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "User");
            }

            ModelState.AddModelError(string.Empty, @Resources.Resource.ErrorWrongData);
            return View(model);
        }

        public ActionResult ChangeCulture(string culture)
        {
            var cookie = Request.Cookies["lang"];
            if (cookie != null)
            {
                cookie.Value = culture;
            }
            else
            {
                cookie = new HttpCookie("lang")
                {
                    HttpOnly = false,
                    Value = culture,
                    Expires = DateTime.Now.AddYears(1)
                };
            }

            Response.Cookies.Add(cookie);
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }

            return RedirectToAction("Login");
        }

        private void SetCredentials()
        {
            void SetDefault()
            {
                userService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                userService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
            }

            if (User != null)
            {
                var claims = User.Identity as ClaimsIdentity;

                var token = claims?.FindFirst("Token")?.Value;
                if (token != null)
                {
                    userService.ClientCredentials.UserName.Password = token;
                }
                else
                {
                    SetDefault();
                }
            }
            else
            {
                SetDefault();
            }
        }
    }
}