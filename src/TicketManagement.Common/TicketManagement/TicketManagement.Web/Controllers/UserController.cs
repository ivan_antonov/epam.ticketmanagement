﻿namespace TicketManagement.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Claims;
    using System.Web;
    using System.Web.Mvc;
    using AutoMapper;
    using Infrastructure.Authentication;
    using Models;
    using PurchaseReference;
    using UserServiceReference;
    using Util;

    public class UserController : Controller
    {
        private readonly PurchaseWCFServiceClient purchaseService;
        private readonly UserWCFServiceClient userService;

        public UserController(PurchaseWCFServiceClient purchaseService, UserWCFServiceClient userService)
        {
            this.purchaseService = purchaseService;
            this.userService = userService;
            SetCredentials();
        }

        [Authorize(Roles = "User")]
        public ActionResult Index(int? page)
        {
            var pageNumber = page ?? 1;

            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = GlobalSettings.SizePage;
            var identity = (ClaimsIdentity)this.User.Identity;
            var id = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.GetAsync($"{GlobalSettings.UrlUserApi.User}/{id}").Result;
            response.EnsureSuccessStatusCode();

            var profile = response.Content.ReadAsAsync<UserProfileModelView>().Result;

            profile.Purchases = Mapper.Map<List<Domain.Models.EventPurchaseDTO>, List<EventPurchaseViewModel>>(purchaseService.GetEventPurchasesById(id));

            return View(profile);
        }

        [Authorize]
        public ActionResult Edit()
        {
            var identity = (ClaimsIdentity)this.User.Identity;
            var id = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.GetAsync($"{GlobalSettings.UrlUserApi.User}/{id}").Result;
            response.EnsureSuccessStatusCode();

            var profile = response.Content.ReadAsAsync<EditProfileModelView>().Result;

            ViewBag.TimeZone = new SelectList(TimeZoneInfo.GetSystemTimeZones(), "Id", "Id", profile.TimeZone);
            //ViewBag.LanguageId = 1; //TODO заменить
            ViewBag.LanguageId = new SelectList(userService.GetLanguages(), "Id", "Name", profile.LanguageId);
            return View(profile);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(EditProfileModelView edit)
        {
            if (ModelState.IsValid)
            {
                var userDto = Mapper.Map<EditProfileModelView, User>(edit);
                var identity = (ClaimsIdentity)this.User.Identity;
                var id = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = httpClient.GetAsync($"{GlobalSettings.UrlUserApi.User}/{id}").Result;
                response.EnsureSuccessStatusCode();

                var profile = response.Content.ReadAsAsync<User>().Result;
                userDto.Id = profile.Id;
                userDto.Balance = profile.Balance;
                userDto.PasswordHash = profile.PasswordHash;
                httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                response = httpClient.PutAsJsonAsync(GlobalSettings.UrlUserApi.User, userDto).Result;
                response.EnsureSuccessStatusCode();
                //TODO убрать
                var culture = userService.GetLanguages().FirstOrDefault(dto => dto.Id == userDto.LanguageId)?.ShortName ?? "ru";

                HttpCookie cookie = Request.Cookies["lang"];
                if (cookie != null)
                {
                    cookie.Value = culture;
                }
                else
                {
                    cookie = new HttpCookie("lang");
                    cookie.HttpOnly = false;
                    cookie.Value = culture;
                    cookie.Expires = DateTime.Now.AddYears(1);
                }

                Response.Cookies.Add(cookie);
                return RedirectToAction("Index");
            }

            ViewBag.TimeZone = new SelectList(TimeZoneInfo.GetSystemTimeZones(), "Id", "Id", edit.TimeZone);
            ViewBag.LanguageId = 1; //TODO заменить
            ViewBag.LanguageId = new SelectList(userService.GetLanguages(), "Id", "Name", edit.LanguageId);
            return View(edit);
        }

        [Authorize(Roles = "User")]
        public ActionResult Pay()
        {
            return View(new PayViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public ActionResult Pay(PayViewModel pay)
        {
            var identity = (ClaimsIdentity)this.User.Identity;
            var id = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.GetAsync($"{GlobalSettings.UrlUserApi.User}/{id}").Result;
            response.EnsureSuccessStatusCode();

            var user = response.Content.ReadAsAsync<User>().Result;
            user.Balance += pay.Value;

            httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            response = httpClient.PutAsJsonAsync(GlobalSettings.UrlUserApi.User, user).Result;
            response.EnsureSuccessStatusCode();

            return RedirectToAction("Index", "User");
        }

        private void SetCredentials()
        {
            void SetDefault()
            {
                purchaseService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                purchaseService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
                userService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                userService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
            }

            if (User != null)
            {
                var claims = User.Identity as ClaimsIdentity;

                var token = claims?.FindFirst("Token")?.Value;
                if (token != null)
                {
                    purchaseService.ClientCredentials.UserName.Password = token;
                    userService.ClientCredentials.UserName.Password = token;
                }
                else
                {
                    SetDefault();
                }
            }
            else
            {
                SetDefault();
            }
        }
    }
}