﻿namespace TicketManagement.Web.Controllers
{
    using System.Web.Mvc;

    public class ErrorController : Controller
    {
        // GET: Error 404s
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        public ActionResult InternalServerError()
        {
            Response.StatusCode = 500;
            return View();
        }
    }
}