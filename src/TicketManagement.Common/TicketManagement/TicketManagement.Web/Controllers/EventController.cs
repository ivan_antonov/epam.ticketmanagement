﻿namespace TicketManagement.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Web.Mvc;
    using AutoMapper;
    using EventReference;
    using Infrastructure.Authentication;
    using Models;
    using PagedList;
    using Util;

    public class EventController : Controller
    {
        private readonly EventWCFServiceClient eventService;

        public EventController(EventWCFServiceClient eventService)
        {
            this.eventService = eventService;
            SetCredentials();
        }

        public ActionResult Index(int? page)
        {
            var pageNumber = page ?? 1;
            SetCredentials();
            IEnumerable<Domain.Models.EventDTO> eventsQuery = eventService.GetAll();

            if (!User.IsInRole("EventManager"))
            {
                eventsQuery = eventsQuery.Where(model => model.Visible);
            }

            var events = Mapper.Map<IEnumerable<Domain.Models.EventDTO>, List<EventViewModel>>(eventsQuery);
            return View(events.OrderBy(model => model.DateStart).ToList().ToPagedList(pageNumber, GlobalSettings.SizePage));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Session["EventId"] = id;
            try
            {
                EventViewModel eventVM;
                if (User.IsInRole("User"))
                {
                    var identity = (ClaimsIdentity)this.User.Identity;
                    var idUser = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;

                    eventVM = Mapper.Map<Domain.Models.EventDTO, EventViewModel>(eventService.GetByUser(id.Value, idUser));
                }
                else
                {
                    eventVM = Mapper.Map<Domain.Models.EventDTO, EventViewModel>(eventService.Get(id.Value));
                }

                if (User.Identity.IsAuthenticated)
                {
                    var identity = (ClaimsIdentity)this.User.Identity;
                    var idUser = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                    var user = WebClient.GetItem<User>($"{GlobalSettings.UrlUserApi.User}/{idUser}");
                    eventVM.DateStart = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(eventVM.DateStart, user.TimeZone);
                    eventVM.DateEnd = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(eventVM.DateEnd, user.TimeZone);
                }

                eventVM.Layout.Areas = eventVM.Layout.Areas.OrderBy(model => model.CoordX).ToList();
                eventVM.Layout.Areas.ForEach(model =>
                {
                    model.Seats = model.Seats.OrderBy(viewModel => viewModel.Row).ToList();
                });
                return View(eventVM);
            }
            catch (NullReferenceException)
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult Search(string search)
        {
            var eventsQuery = eventService.GetAll().Where(dto => dto.Name.Contains(search));
            var events = Mapper.Map<IEnumerable<Domain.Models.EventDTO>, List<EventViewModel>>(eventsQuery);
            return View("Index", events.OrderBy(model => model.DateStart).ToList().ToPagedList(1, GlobalSettings.SizePage));
        }

        private void SetCredentials()
        {
            void SetDefault()
            {
                this.eventService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                this.eventService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
            }

            if (User != null)
            {
                var claims = User.Identity as ClaimsIdentity;

                var token = claims?.FindFirst("Token")?.Value;
                if (token != null)
                {
                    this.eventService.ClientCredentials.UserName.Password = token;
                }
                else
                {
                    SetDefault();
                }
            }
            else
            {
                SetDefault();
            }
        }
    }
}