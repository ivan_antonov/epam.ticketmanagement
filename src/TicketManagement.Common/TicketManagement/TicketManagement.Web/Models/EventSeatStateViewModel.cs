﻿// <copyright file="EventSeatDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Web.Models
{
    /// <summary>
    /// EventSeat
    /// </summary>
    public class EventSeatStateViewModel
    {

        public int Id { get; set; }

        public bool State { get; set; }
    }
}