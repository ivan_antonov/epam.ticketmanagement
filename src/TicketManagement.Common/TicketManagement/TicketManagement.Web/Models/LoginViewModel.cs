﻿namespace TicketManagement.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class LoginViewModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [System.Web.Mvc.HiddenInput(DisplayValue = false)]
        public string ReturnUrl { get; set; }
    }
}