﻿namespace TicketManagement.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class UserProfileModelView
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public int Language { get; set; }

        public string TimeZone { get; set; }

        [DisplayFormat(DataFormatString = "{0:C0}")]
        [DataType(DataType.Currency)]
        public decimal Balance { get; set; }

        public List<EventPurchaseViewModel> Purchases { get; set; }
    }
}