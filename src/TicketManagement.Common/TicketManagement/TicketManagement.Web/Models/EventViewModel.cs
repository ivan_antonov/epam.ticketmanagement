﻿namespace TicketManagement.Web.Models
{
    using System;

    public class EventViewModel
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets layout
        /// </summary>
        public EventLayoutViewModel Layout { get; set; }

        /// <summary>
        /// Gets or sets date start
        /// </summary>
        public DateTimeOffset DateStart { get; set; }

        /// <summary>
        /// Gets or sets date end
        /// </summary>
        public DateTimeOffset DateEnd { get; set; }

        /// <summary>
        /// Gets or sets poster
        /// </summary>
        public string Poster { get; set; }

        /// <summary>
        /// Gets or sets visible
        /// </summary>
        public bool Visible { get; set; }
    }
}