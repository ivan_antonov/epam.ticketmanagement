﻿namespace TicketManagement.Web.Models
{
    public class LayoutViewModel
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets venue
        /// </summary>
        public VenueViewModel Venue { get; set; }
    }
}