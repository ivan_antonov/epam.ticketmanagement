﻿namespace TicketManagement.Web.Models
{
    public class VenueViewModel
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets  address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets phone
        /// </summary>
        public string Phone { get; set; }
    }
}