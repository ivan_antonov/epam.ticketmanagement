﻿namespace TicketManagement.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class EditProfileModelView
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public int LanguageId { get; set; }

        public string TimeZone { get; set; }
    }
}