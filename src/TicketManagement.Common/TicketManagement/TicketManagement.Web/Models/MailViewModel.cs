﻿namespace TicketManagement.Web.Models
{
    public class MailViewModel
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }
    }
}