﻿namespace TicketManagement.Web.Models
{
    public class PayViewModel
    {
        public decimal Value { get; set; }
        public string Expiry { get; set; }
        public string CVC { get; set; }
        public string NameCard { get; set; }
    }
}