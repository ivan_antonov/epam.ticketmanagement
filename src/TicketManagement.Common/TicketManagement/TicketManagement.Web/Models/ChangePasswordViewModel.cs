﻿namespace TicketManagement.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ChangePasswordViewModel
    {
        [DataType(DataType.Password)]
        [MinLength(6)]
        public string CurrentPassword { get; set; }

        [DataType(DataType.Password)]
        [MinLength(6)]
        public string NewPassword { get; set; }
    }
}