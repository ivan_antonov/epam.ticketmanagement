﻿// <copyright file="EventSeatDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Web.Models
{
    /// <summary>
    /// EventSeat
    /// </summary>
    public class EventSeatViewModel
    {

        public int Id { get; set; }

        public int EventAreaId { get; set; }

        public int Row { get; set; }

        public int Number { get; set; }

        public int State { get; set; }
    }
}