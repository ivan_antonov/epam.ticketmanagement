﻿namespace TicketManagement.Web.Models
{
    using System;

    public class EventPurchaseViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Poster { get; set; }

        public DateTimeOffset DatePurchase { get; set; }

        public decimal Price { get; set; }
    }
}