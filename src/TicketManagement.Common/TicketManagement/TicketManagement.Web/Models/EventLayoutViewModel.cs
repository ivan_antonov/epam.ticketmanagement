﻿// <copyright file="LayoutDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Web.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Layout
    /// </summary>
    public class EventLayoutViewModel
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets id of venue
        /// </summary>
        public int VenueId { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets areas
        /// </summary>
        public List<EventAreaViewModel> Areas { get; set; }

        /// <summary>
        /// Gets or sets venue
        /// </summary>
        public VenueViewModel Venue { get; set; }
    }
}
