﻿namespace TicketManagement.Web.Util
{
    using System.Configuration;
    using Infrastructure.ConfigSections;

    internal static class GlobalSettings
    {
        public static int SizePage { get; } = 8;

        public static UrlEndPoints UrlUserApi => (UrlEndPoints)ConfigurationManager.GetSection("urlEndPoints");

        public static string UserNameDefault { get; set; } = "TicketManagement";

        public static string UserPsswordDefault { get; set; } = "TicketManagement";
    }
}