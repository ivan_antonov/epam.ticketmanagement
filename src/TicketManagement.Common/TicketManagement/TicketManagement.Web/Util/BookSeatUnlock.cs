﻿namespace TicketManagement.Web.Util
{
    using System;
    using System.Linq;
    using BookSeatReference;

    internal class BookSeatUnlock
    {

        private readonly BookSeatWCFServiceClient bookSeatService;

        public BookSeatUnlock(BookSeatWCFServiceClient bookSeatService)
        {
            this.bookSeatService = bookSeatService;
            this.bookSeatService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
            this.bookSeatService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
        }

        public void Execute(int minutes)
        {
            var seats = bookSeatService.GetAll().ToList();
            foreach (var bookSeatDto in seats)
            {
                var interval = DateTimeOffset.Now - bookSeatDto.Time;
                if (interval.TotalMinutes > minutes)
                {
                    bookSeatService.Delete(bookSeatDto.Id);
                }
            }
        }

    }
}