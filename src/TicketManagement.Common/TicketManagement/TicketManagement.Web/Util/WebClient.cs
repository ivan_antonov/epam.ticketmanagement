﻿namespace TicketManagement.Web.Util
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Infrastructure.Authentication;
    using Models;

    public static class WebClient
    {
        public static T GetItem<T>(string url)
        {
            var response = Request(url);
            var result = response.Content.ReadAsAsync<T>().Result;
            return result;
        }

        public static IEnumerable<T> GetItems<T>(string url)
        {
            var response = Request(url);
            var result = response.Content.ReadAsAsync<IEnumerable<T>>().Result;
            return result;
        }

        public static void PutItem<T>(string url, T item)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.PutAsJsonAsync(url, item).Result;
            response.EnsureSuccessStatusCode();
        }

        public static void PostItem<T>(string url, T item)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.PostAsJsonAsync(url, item).Result;
            response.EnsureSuccessStatusCode();
        }

        public static string GetToken(string url, LoginViewModel model)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var message = new Dictionary<string, string>()
            {
                {"grant_type", "password"},
                {"username", model.UserName},
                {"password", model.Password }
            };

            var response = httpClient.PostAsync(url, new FormUrlEncodedContent(message)).Result;
            var result = response.Content.ReadAsAsync<TokenModel>().Result;
            string token = result.access_token;

            return token;
        }

        private static HttpResponseMessage Request(string url)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.GetAsync(url).Result;
            response.EnsureSuccessStatusCode();
            return response;
        }
    }
}