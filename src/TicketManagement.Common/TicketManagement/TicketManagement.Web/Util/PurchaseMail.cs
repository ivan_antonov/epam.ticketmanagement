﻿namespace TicketManagement.Web.Util
{
    using System.Collections.Generic;
    using Domain.Models;

    public static class PurchaseMail
    {
        public static MailPurchaseDTO GetMailModel(MailPurchaseDTO model, IEnumerable<string> infoSeats)
        {
            model.Subject = Resources.Resource.SubjectForMail;
            model.Text = $"<h2>{model.UserName}</h2> {Resources.Resource.InfoMail} <table>";
            foreach (var infoSeat in infoSeats)
            {
                model.Text += $"<tr><td>{infoSeat}</td></tr>";
            }

            model.Text += "</table>";

            return model;
        }
    }
}