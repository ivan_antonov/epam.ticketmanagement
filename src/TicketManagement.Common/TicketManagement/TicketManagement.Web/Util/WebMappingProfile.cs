﻿// <copyright file="BLMappingProfile.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Web.Util
{
    using Areas.Manager.Models;
    using AutoMapper;
    using Infrastructure.Authentication;
    using Models;

    public class WebMappingProfile : Profile
    {
        public WebMappingProfile()
        {
            CreateMap<Domain.Models.EventDTO, EventViewModel>().ReverseMap();
            CreateMap<Domain.Models.EventDTO, EventCreateViewModel>().ReverseMap();
            CreateMap<Domain.Models.EventDTO, EventEditViewModel>().ReverseMap();
            CreateMap<Domain.Models.EventAreaDTO, EventAreaViewModel>().ReverseMap();
            CreateMap<Domain.Models.EventLayoutDTO, EventLayoutViewModel>().ReverseMap();
            CreateMap<Domain.Models.LayoutDTO, Models.LayoutViewModel>().ReverseMap();
            CreateMap<Domain.Models.LayoutDTO, Areas.Manager.Models.LayoutViewModel>().ReverseMap();
            CreateMap<Domain.Models.EventPurchaseDTO, EventPurchaseViewModel>().ReverseMap();
            CreateMap<Domain.Models.BookSeatDTO, BookSeatViewModel>().ReverseMap();

            CreateMap<User, EditProfileModelView>().ReverseMap();
        }
    }
}
