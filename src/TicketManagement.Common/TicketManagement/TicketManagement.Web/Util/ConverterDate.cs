﻿namespace TicketManagement.Web.Util
{
    using System;

    public static class ConverterDate
    {
        public static DateTime AddTimeToDate(DateTime date, DateTime time)
        {
            date = date.AddHours(time.Hour);
            date = date.AddMinutes(time.Minute);

            return date;
        }
    }
}