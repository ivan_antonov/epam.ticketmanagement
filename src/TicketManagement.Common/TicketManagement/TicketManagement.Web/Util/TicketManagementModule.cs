﻿namespace TicketManagement.Web.Util
{
    using AreaReference;
    using BookSeatReference;
    using EventReference;
    using Hangfire;
    using Infrastructure.Authentication;
    using LayoutReference;
    using Microsoft.AspNet.Identity;
    using Ninject.Modules;
    using PurchaseReference;
    using SeatReference;
    using UserServiceReference;
    using VenueServiceReference;

    public class TicketManagementModule : NinjectModule
    {
        public override void Load()
        {
            Bind<VenueWCFServiceClient>().ToSelf();
            Bind<SeatWCFServiceClient>().ToSelf();
            Bind<PurchaseWCFServiceClient>().ToSelf();
            Bind<LayoutWCFServiceClient>().ToSelf();
            Bind<EventWCFServiceClient>().ToSelf();
            Bind<BookSeatWCFServiceClient>().ToSelf();
            Bind<AreaWCFServiceClient>().ToSelf();
            Bind<UserWCFServiceClient>().ToSelf();

            Bind<IUserStore<User, string>>().To<CustomUserStore>();
            Bind<UserManager<User, string>>().ToSelf();

            Bind<BookSeatUnlock>().ToSelf().InBackgroundJobScope();
        }
    }
}