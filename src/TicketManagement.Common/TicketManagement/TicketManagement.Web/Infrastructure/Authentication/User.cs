﻿namespace TicketManagement.Web.Infrastructure.Authentication
{
    using Microsoft.AspNet.Identity;

    public class User : IUser<string>
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public int LanguageId { get; set; }

        public string TimeZone { get; set; }

        public decimal Balance { get; set; }
    }
}