﻿namespace TicketManagement.Web.Infrastructure.Authentication
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Util;

    public class CustomUserStore : IUserClaimStore<User>, IUserPasswordStore<User>
    {
        public Task AddClaimAsync(User user, Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task CreateAsync(User user)
        {
            user.Id = Guid.NewGuid().ToString();

            WebClient.PostItem(GlobalSettings.UrlUserApi.User, user);
            return Task.FromResult(0);
        }

        public Task DeleteAsync(User user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }

        public Task<User> FindByIdAsync(string userId)
        {
            var result = WebClient.GetItem<User>($"{GlobalSettings.UrlUserApi.User}/{userId}");
            return Task.FromResult(result);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            var result = WebClient.GetItem<User>($"{GlobalSettings.UrlUserApi.User}?userName={userName}");
            return Task.FromResult(result);
        }

        public Task<IList<Claim>> GetClaimsAsync(User user)
        {
            var roles = WebClient.GetItems<string>($"{GlobalSettings.UrlUserApi.Role}?userName={user.UserName}");
            IList<Claim> claims = new List<Claim>();
            foreach (var role in roles.ToList())
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            return Task.FromResult(claims);
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            var result = WebClient.GetItem<User>($"{GlobalSettings.UrlUserApi.User}/{user.Id}");
            return Task.FromResult(result.PasswordHash != null);
        }

        public Task RemoveClaimAsync(User user, Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task UpdateAsync(User user)
        {
            WebClient.PutItem(GlobalSettings.UrlUserApi.User, user);
            return Task.FromResult(0);
        }
    }
}