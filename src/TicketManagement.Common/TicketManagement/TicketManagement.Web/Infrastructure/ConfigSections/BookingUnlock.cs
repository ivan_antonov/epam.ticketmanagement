﻿namespace TicketManagement.Web.Infrastructure.ConfigSections
{
    using System.Configuration;

    public class BookingUnlock : ConfigurationSection
    {
        [ConfigurationProperty(
            "minutes",
            DefaultValue = 15)]
        public int Minutes
        {
            get { return (int)base["minutes"]; }
            set { base["minutes"] = value; }
        }
    }
}