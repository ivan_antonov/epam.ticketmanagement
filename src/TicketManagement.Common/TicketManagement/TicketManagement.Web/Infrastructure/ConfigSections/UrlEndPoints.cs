﻿namespace TicketManagement.Web.Infrastructure.ConfigSections
{
    using System.Configuration;

    public class UrlEndPoints : ConfigurationSection
    {
        [ConfigurationProperty("user")]
        public string User => base["user"].ToString();

        [ConfigurationProperty("role")]
        public string Role => base["role"].ToString();

        [ConfigurationProperty("token")]
        public string Token => base["token"].ToString();
    }
}