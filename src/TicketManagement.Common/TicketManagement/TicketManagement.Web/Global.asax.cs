﻿namespace TicketManagement.Web
{
    using System;
    using System.Configuration;
    using System.Globalization;
    using System.Threading;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using AutoMapper;
    using Hangfire;
    using Infrastructure.ConfigSections;
    using Ninject;
    using Ninject.Modules;
    using Ninject.Web.Mvc;
    using Util;
    using WebApplication1;

    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly string connectionString = "TestConnection";

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // inject dependency
            NinjectModule ticketModule = new TicketManagementModule();

            var kernel = new StandardKernel(ticketModule);
            kernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
            Mapper.Initialize(cfg => cfg.AddProfile(new WebMappingProfile()));

            // config HangFire
            GlobalConfiguration.Configuration.UseNinjectActivator(kernel);
            GlobalConfiguration.Configuration.UseSqlServerStorage(connectionString);
            var time = (BookingUnlock)ConfigurationManager.GetSection("bookingUnlock");
            RecurringJob.AddOrUpdate(() => kernel.Get<BookSeatUnlock>().Execute(time.Minutes), Cron.Minutely);
        }

        protected void Application_BeginRequest()
        {
            try
            {
                string cultureName = null;
                var cultureCookie = HttpContext.Current.Request.Cookies["lang"];
                cultureName = cultureCookie != null ? cultureCookie.Value : "ru";
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);
            }
            catch (Exception)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru");
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru");
            }
        }
    }
}
