﻿$(function () {
    $("#DateStart")
        .datepicker({ dateFormat: 'm/d/yy' })
        .get(0).setAttribute("type", "text");
    $("#DateEnd")
        .datepicker({ dateFormat: 'm/d/yy' })
        .get(0).setAttribute("type", "text");
});