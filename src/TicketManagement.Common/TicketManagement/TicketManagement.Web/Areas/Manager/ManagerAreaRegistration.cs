﻿namespace TicketManagement.Web.Areas.Manager
{
    using System.Web.Mvc;

    public class ManagerAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Manager";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Manager_default",
                "Manager/{controller}/{action}/{id}",
                new { controller = "Event", action = "Create", id = UrlParameter.Optional }
            );
        }
    }
}