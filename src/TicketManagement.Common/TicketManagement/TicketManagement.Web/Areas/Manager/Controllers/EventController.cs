﻿namespace TicketManagement.Web.Areas.Manager.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.ServiceModel;
    using System.Web.Mvc;
    using AutoMapper;
    using BookSeatReference;
    using Domain.Models;
    using EventReference;
    using Infrastructure.Authentication;
    using LayoutReference;
    using MailServiceReference;
    using Models;
    using PurchaseReference;
    using Util;
    using VenueServiceReference;
    using Web.Models;

    public class EventController : Controller
    {
        private readonly EventWCFServiceClient eventService;
        private readonly VenueWCFServiceClient venueService;
        private readonly LayoutWCFServiceClient layoutService;
        private readonly BookSeatWCFServiceClient bookSeatService;
        private readonly PurchaseWCFServiceClient purchaseService;
        private readonly MailWCFServiceClient mailService;

        public EventController(
            EventWCFServiceClient eventService,
            VenueWCFServiceClient venueService,
            LayoutWCFServiceClient layoutService,
            BookSeatWCFServiceClient bookSeatService,
            PurchaseWCFServiceClient purchaseService,
            MailWCFServiceClient mailService)
        {
            this.eventService = eventService;
            this.venueService = venueService;
            this.layoutService = layoutService;
            this.bookSeatService = bookSeatService;
            this.purchaseService = purchaseService;
            this.mailService = mailService;

            SetCredentials();
        }

        [Authorize(Roles = "EventManager")]
        public ActionResult Create(int? page)
        {
            var pageNumber = page ?? 1;

            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = GlobalSettings.SizePage;

            var eventViewModel = new EventCreateViewModel
            {
                Venues = Mapper.Map<IList<Domain.Models.VenueDTO>, List<VenueViewModel>>(venueService.GetAll())
            };

            return View(eventViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "EventManager")]
        public ActionResult Create(EventCreateViewModel eventViewModel)
        {
            var identity = (ClaimsIdentity)this.User.Identity;
            var id = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            var user = WebClient.GetItem<User>($"{GlobalSettings.UrlUserApi.User}/{id}");

            if (ModelState.IsValid)
            {
                try
                {
                    var zone = TimeZoneInfo.FindSystemTimeZoneById(user.TimeZone);
                    var eventDto = Mapper.Map<EventCreateViewModel, Domain.Models.EventDTO>(eventViewModel);
                    var date = ConverterDate.AddTimeToDate(eventDto.DateStart.DateTime, eventViewModel.TimeStart);
                    eventDto.DateStart = new DateTimeOffset(date, zone.BaseUtcOffset);
                    date = ConverterDate.AddTimeToDate(eventDto.DateEnd.DateTime, eventViewModel.TimeEnd);
                    eventDto.DateEnd = new DateTimeOffset(date, zone.BaseUtcOffset);
                    eventService.Create(eventDto);
                    return RedirectToAction("Index", new { area = string.Empty, controller = "Event" });
                }
                catch (FaultException<Domain.Models.ServiceExeption> e)
                {
                    switch (e.Detail.Message)
                    {
                        case "InvalidDateException":
                            {
                                ModelState.AddModelError(string.Empty, @Resources.Resource.ErrorInvalidDate);
                                break;
                            }

                        case "UniqueArgumentException":
                            {
                                ModelState.AddModelError(string.Empty, @Resources.Resource.ErrorUnique);
                                break;
                            }

                        case "SeatBusyException":
                            {
                                ModelState.AddModelError(string.Empty, @Resources.Resource.ErrorSeatBusy);
                                break;
                            }
                    }
                }
            }

            ViewBag.PageNumber = 1;
            ViewBag.PageSize = GlobalSettings.SizePage;
            eventViewModel.Venues =
                Mapper.Map<IList<Domain.Models.VenueDTO>, List<VenueViewModel>>(venueService.GetAll());
            ModelState.AddModelError(string.Empty, @Resources.Resource.ChooseLayout);
            return View(eventViewModel);
        }

        [Authorize(Roles = "EventManager")]
        public ActionResult Edit(int id)
        {
            var eventDto = eventService.Get(id);
            if (eventDto == null)
            {
                HttpNotFound();
            }

            var eventVM = Mapper.Map<Domain.Models.EventDTO, EventEditViewModel>(eventDto);
            eventVM.Venues = Mapper.Map<IList<Domain.Models.VenueDTO>, List<VenueViewModel>>(venueService.GetAll());
            return View(eventVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "EventManager")]
        public ActionResult Edit(EventEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var identity = (ClaimsIdentity)this.User.Identity;
                var id = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                var user = WebClient.GetItem<User>($"{GlobalSettings.UrlUserApi.User}/{id}");

                try
                {
                    var zone = TimeZoneInfo.FindSystemTimeZoneById(user.TimeZone);
                    var eventDto = Mapper.Map<EventEditViewModel, Domain.Models.EventDTO>(model);
                    var date = ConverterDate.AddTimeToDate(eventDto.DateStart.DateTime, model.TimeStart);
                    eventDto.DateStart = new DateTimeOffset(date, zone.BaseUtcOffset);
                    date = ConverterDate.AddTimeToDate(eventDto.DateEnd.DateTime, model.TimeEnd);
                    eventDto.DateEnd = new DateTimeOffset(date, zone.BaseUtcOffset);
                    eventService.Update(eventDto);
                    return RedirectToAction("Index", new { area = string.Empty, controller = "Event" });
                }
                catch (FaultException<Domain.Models.ServiceExeption> e)
                {
                    switch (e.Detail.Message)
                    {
                        case "InvalidDateException":
                            {
                                ModelState.AddModelError(string.Empty, @Resources.Resource.ErrorInvalidDate);
                                break;
                            }

                        case "UniqueArgumentException":
                            {
                                ModelState.AddModelError(string.Empty, @Resources.Resource.ErrorUnique);
                                break;
                            }

                        case "SeatBusyException":
                            {
                                ModelState.AddModelError(string.Empty, @Resources.Resource.ErrorSeatBusy);
                                break;
                            }
                    }
                }
            }

            model.Layout =
                Mapper.Map<Domain.Models.EventLayoutDTO, EventLayoutViewModel>(eventService.Get(model.Id).Layout);
            model.Venues = Mapper.Map<IList<Domain.Models.VenueDTO>, List<VenueViewModel>>(venueService.GetAll());
            ModelState.AddModelError(string.Empty, @Resources.Resource.CheckYourData);
            return View(model);
        }

        [Authorize(Roles = "EventManager")]
        public ActionResult EditPrice(int id)
        {
            var eventArea = eventService.GetAllArea().FirstOrDefault(dto => dto.Id == id);
            if (eventArea == null)
            {
                HttpNotFound();
            }

            var eventAreaVM = Mapper.Map<Domain.Models.EventAreaDTO, EventAreaViewModel>(eventArea);

            return View(eventAreaVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "EventManager")]
        public ActionResult EditPrice(EventAreaViewModel model)
        {
            var eventAreaDto = Mapper.Map<EventAreaViewModel, Domain.Models.EventAreaDTO>(model);
            eventService.UpdateArea(eventAreaDto);
            return RedirectToAction("Index", new { area = string.Empty, controller = "Event" });
        }

        public ActionResult GetLayouts(int id)
        {
            var layouts = layoutService.GetAllByVenueId(id);
            var layoutsViewModel = Mapper.Map<IList<Domain.Models.LayoutDTO>, List<Models.LayoutViewModel>>(layouts);
            return PartialView("_LayoutList", layoutsViewModel);
        }

        [HttpPost]
        public JsonResult Upload()
        {
            var fullPath = "/Content/images/";
            foreach (string file in Request.Files)
            {
                var upload = Request.Files[file];
                if (upload != null)
                {
                    var fileName = System.IO.Path.GetFileName(upload.FileName);
                    fullPath += fileName;
                    upload.SaveAs(Server.MapPath("~/Content/images/" + fileName));
                }
            }

            return Json(fullPath);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public ActionResult LockSeat(EventSeatStateViewModel seatViewModel)
        {
            var identity = (ClaimsIdentity)this.User.Identity;
            var id = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            if (seatViewModel != null)
            {
                if (seatViewModel.State)
                {
                    var bookSeat = new Domain.Models.BookSeatDTO
                    {
                        EventSeatId = seatViewModel.Id,
                        UserId = id,
                        Time = DateTimeOffset.Now
                    };
                    var isCheck = bookSeatService
                        .GetAll()
                        .FirstOrDefault(dto => dto.UserId == id && dto.EventSeatId == seatViewModel.Id);
                    if (isCheck != null)
                    {
                        bookSeatService.Delete(isCheck.Id);
                    }

                    bookSeatService.Create(bookSeat);
                }
                else
                {
                    var seat = bookSeatService.GetAll().FirstOrDefault(dto => dto.EventSeatId == seatViewModel.Id);
                    if (seat != null)
                    {
                        bookSeatService.Delete(seat.Id);
                    }
                }
            }

            var seats = Mapper.Map<IQueryable<Domain.Models.BookSeatDTO>, List<BookSeatViewModel>>(bookSeatService
                .GetAll()
                .AsQueryable()
                .Where(dto => dto.UserId == id));

            return PartialView("_CartView", seats);
        }

        public ActionResult BuySeats()
        {
            try
            {
                var identity = (ClaimsIdentity)this.User.Identity;
                var userId = (string)identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                var eventId = (int)Session["eventId"];

                var user = WebClient.GetItem<User>($"{GlobalSettings.UrlUserApi.User}/{userId}");
                var seats = Mapper.Map<IQueryable<Domain.Models.BookSeatDTO>, List<BookSeatViewModel>>(bookSeatService
                    .GetAll()
                    .AsQueryable()
                    .Where(dto => dto.UserId == userId)).Select(c =>
                    $"{Resources.Resource.Row}:{c.Row} {Resources.Resource.Number}:{c.Number} {Resources.Resource.Price}: {c.Price}");

                var mailModel = new MailPurchaseDTO
                {
                    UserName = user.UserName,
                    Email = user.Email
                };

                purchaseService.BuySeats(eventId, userId);
                mailModel = PurchaseMail.GetMailModel(mailModel, seats);
                mailService.SendAsync(mailModel);

                return PartialView("_SuccedView", Resources.Resource.SucceedBuy);
            }
            catch (FaultException<Domain.Models.ServiceExeption> e)
            {
                if (e.Detail.Message == "NotEnoughFundsException")
                {
                    return PartialView("_ErrorView", Resources.Resource.ErrorPriceException);
                }

                return PartialView("_ErrorView", Resources.Resource.ErrorInternalError);
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                eventService.Delete(id);
            }
            catch (FaultException<Domain.Models.ServiceExeption> e)
            {
                if (e.Detail.Message == "SeatBusyException")
                {
                    return PartialView("_ErrorView", Resources.Resource.ErrorDeleteEvent);
                }
            }

            return PartialView("_ErrorView", string.Empty);
        }

        private void SetCredentials()
        {
            void SetDefault()
            {
                eventService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                eventService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
                venueService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                venueService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
                layoutService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                layoutService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
                bookSeatService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                bookSeatService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
                purchaseService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                purchaseService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
                mailService.ClientCredentials.UserName.UserName = GlobalSettings.UserNameDefault;
                mailService.ClientCredentials.UserName.Password = GlobalSettings.UserPsswordDefault;
            }

            if (User != null)
            {
                var claims = User.Identity as ClaimsIdentity;

                var token = claims?.FindFirst("Token")?.Value;
                if (token != null)
                {
                    eventService.ClientCredentials.UserName.Password = token;
                    venueService.ClientCredentials.UserName.Password = token;
                    layoutService.ClientCredentials.UserName.Password = token;
                    bookSeatService.ClientCredentials.UserName.Password = token;
                    purchaseService.ClientCredentials.UserName.Password = token;
                    mailService.ClientCredentials.UserName.Password = token;
                }
                else
                {
                    SetDefault();
                }
            }
            else
            {
                SetDefault();
            }
        }
    }
}