﻿namespace TicketManagement.Web.Areas.Manager.Models
{
    public class LayoutViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
