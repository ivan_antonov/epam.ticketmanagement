﻿namespace TicketManagement.Web.Areas.Manager.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Web.Models;

    public class EventCreateViewModel
    {
        [System.Web.Mvc.HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        [MaxLength(120)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [System.Web.Mvc.HiddenInput(DisplayValue = false)]
        [Required]
        public int LayoutId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTimeOffset DateStart { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTimeOffset DateEnd { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public DateTime TimeStart { get; set; }

        [Required]
        [DataType(DataType.Time)]
        public DateTime TimeEnd { get; set; }

        [Required]
        public string Poster { get; set; }

        public List<VenueViewModel> Venues { get; set; }
    }
}