﻿namespace TicketManagement.Web.Areas.Manager.Models
{
    public class BookSeatViewModel
    {
        public int Id { get; set; }

        public int Row { get; set; }

        public int Number { get; set; }

        public decimal Price { get; set; }
    }
}