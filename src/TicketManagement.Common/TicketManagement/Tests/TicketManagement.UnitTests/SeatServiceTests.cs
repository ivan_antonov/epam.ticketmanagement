﻿namespace TicketManagement.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using BLL.DTO;
    using BLL.Infrastructure;
    using BLL.Interfaces;
    using BLL.Services;
    using DAL.Entities;
    using DAL.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class SeatServiceTests
    {
        private ISeatService service;
        private Mock<IBaseRepository<Seat>> mock;

        [SetUp]
        public void Init()
        {
            mock = new Mock<IBaseRepository<Seat>>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BLMappingProfile());
            });
            IMapper mapper = new Mapper(config);
            service = new SeatService(mapper, mock.Object);
        }

        [Test]
        public void GetSeatTest()
        {
            // Arrange
            mock.Setup(db => db.Get(It.IsAny<int>()))
                .Returns(new Seat());

            // Act
            service.Get(2);

            // Assert
            mock.Verify(verify => verify.Get(It.IsAny<int>()));
        }

        [Test]
        public void GetSeatNotExistTest()
        {
            mock.Setup(a => a.Get(It.IsAny<int>())).Returns((Seat)null);

            Assert.Throws<NullReferenceException>(() => service.Get(5));
        }

        [Test]
        public void GetSeatsTest()
        {
            // Arrange
            mock.Setup(a => a.GetAll())
                .Returns(new List<Seat>().AsQueryable());

            // Act
            service.GetAll();

            // Assert
            mock.Verify(verify => verify.GetAll());
        }

        [Test]
        public void CreateSeatTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Seat>().AsQueryable());
            var seat = new SeatDTO()
            {
                AreaId = 1,
                Number = 2,
                Row = 3
            };
            Assert.DoesNotThrow(() => service.Create(seat));
        }

        [Test]
        public void CreateSeatNullTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Seat>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Create(null));
        }

        [Test]
        public void CreateSeatNameUniqueTest()
        {
            var seatDto = new SeatDTO()
            {
                AreaId = 1,
                Number = 2,
                Row = 3
            };
            var seat = new Seat()
            {
                AreaId = 1,
                Number = 2,
                Row = 3
            };
            mock.Setup(a => a.GetAll()).Returns(new List<Seat>() { seat }.AsQueryable());
            mock.Setup(a => a.Create(It.IsAny<Seat>()));
            Assert.That(
                () => { service.Create(seatDto); },
                Throws.Exception
                    .TypeOf<UniqueArgumentException>()
                    .With.Property("Message")
                    .EqualTo("The number must be unique"));
        }

        [Test]
        public void UpdateSeatTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Seat>().AsQueryable());
            var seat = new SeatDTO()
            {
                Id = 1,
                AreaId = 1,
                Number = 2,
                Row = 3
            };
            Assert.DoesNotThrow(() => service.Update(seat));
        }

        [Test]
        public void UpdateSeatNullTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Seat>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Update(null));
        }

        [Test]
        public void UpdateSeatNameUniqueTest()
        {
            var seatDto = new SeatDTO()
            {
                Id = 1,
                AreaId = 1,
                Number = 2,
                Row = 3
            };
            var seat = new Seat()
            {
                Id = 1,
                AreaId = 1,
                Number = 2,
                Row = 3
            };
            mock.Setup(a => a.GetAll()).Returns(new List<Seat>() { seat }.AsQueryable());
            mock.Setup(a => a.Update(It.IsAny<Seat>()));
            Assert.That(
                () => { service.Update(seatDto); },
                Throws.Exception
                    .TypeOf<UniqueArgumentException>()
                    .With.Property("Message")
                    .EqualTo("The number must be unique"));
        }

        [Test]
        public void DeleteSeat()
        {
            // Arrange
            mock.Setup(a => a.Get(It.IsAny<int>()))
                .Returns((Seat)null);

            // Act
            service.Delete(2);

            // Assert
            mock.Verify(verify => verify.Delete(It.IsAny<int>()));
        }
    }
}
