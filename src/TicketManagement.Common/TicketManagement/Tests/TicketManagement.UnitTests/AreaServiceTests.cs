﻿namespace TicketManagement.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using BLL.DTO;
    using BLL.Infrastructure;
    using BLL.Interfaces;
    using BLL.Services;
    using DAL.Entities;
    using DAL.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class AreaServiceTests
    {
        private IAreaService service;
        private Mock<IBaseRepository<Area>> mock;

        [SetUp]
        public void Init()
        {
            mock = new Mock<IBaseRepository<Area>>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BLMappingProfile());
            });
            IMapper mapper = new Mapper(config);
            service = new AreaService(mapper, mock.Object);
        }

        [Test]
        public void GetAreaTest()
        {
            // Arrange
            mock.Setup(db => db.Get(It.IsAny<int>()))
                .Returns(new Area());

            // Act
            service.Get(2);

            // Assert
            mock.Verify(verify => verify.Get(It.IsAny<int>()));
        }

        [Test]
        public void GetAreaNotExistTest()
        {
            mock.Setup(a => a.Get(It.IsAny<int>())).Returns((Area)null);

            Assert.Throws<NullReferenceException>(() => service.Get(5));
        }

        [Test]
        public void GetAreasTest()
        {
            // Arrange
            mock.Setup(a => a.GetAll())
                .Returns(new List<Area>().AsQueryable());

            // Act
            service.GetAll();

            // Assert
            mock.Verify(verify => verify.GetAll());
        }

        [Test]
        public void CreateAreaTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Area>().AsQueryable());
            var area = new AreaDTO()
            {
                Description = "something description",
                LayoutId = 1,
                CoordX = 0,
                CoordY = 0
            };
            Assert.DoesNotThrow(() => service.Create(area));
        }

        [Test]
        public void CreateAreaNullTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Area>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Create(null));
        }

        [Test]
        public void CreateAreaNameUniqueTest()
        {
            var areaDto = new AreaDTO()
            {
                Description = "something description",
                LayoutId = 1,
                CoordX = 0,
                CoordY = 0
            };
            var area = new Area()
            {
                Description = "something description",
                LayoutId = 1,
                CoordX = 0,
                CoordY = 0
            };
            mock.Setup(a => a.GetAll()).Returns(new List<Area>() { area }.AsQueryable());
            mock.Setup(a => a.Create(It.IsAny<Area>()));
            Assert.That(
                () => { service.Create(areaDto); },
                Throws.Exception
                    .TypeOf<UniqueArgumentException>()
                    .With.Property("Message")
                    .EqualTo("The description must be unique"));
        }

        [Test]
        public void UpdateAreaTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Area>().AsQueryable());
            var area = new AreaDTO()
            {
                Description = "something description",
                LayoutId = 1,
                CoordX = 0,
                CoordY = 0
            };
            Assert.DoesNotThrow(() => service.Create(area));
        }

        [Test]
        public void UpdateAreaNullTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Area>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Create(null));
        }

        [Test]
        public void UpdateAreaNameUniqueTest()
        {
            var areaDto = new AreaDTO()
            {
                Description = "something description is updated",
                LayoutId = 1,
                CoordX = 0,
                CoordY = 0
            };
            var area = new Area()
            {
                Description = "something description is updated",
                LayoutId = 1,
                CoordX = 0,
                CoordY = 0
            };
            mock.Setup(a => a.GetAll()).Returns(new List<Area>() { area }.AsQueryable());
            mock.Setup(a => a.Create(It.IsAny<Area>()));
            Assert.That(
                () => { service.Create(areaDto); },
                Throws.Exception
                    .TypeOf<UniqueArgumentException>()
                    .With.Property("Message")
                    .EqualTo("The description must be unique"));
        }

        [Test]
        public void DeleteArea()
        {
            // Arrange
            mock.Setup(a => a.Get(It.IsAny<int>()))
                .Returns((Area)null);

            // Act
            service.Delete(2);

            // Assert
            mock.Verify(verify => verify.Delete(It.IsAny<int>()));
        }
    }
}
