﻿namespace TicketManagement.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using BLL.DTO;
    using BLL.Infrastructure;
    using BLL.Interfaces;
    using BLL.Services;
    using DAL.Entities;
    using DAL.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class LayoutServiceTests
    {
        private ILayoutService service;
        private Mock<IBaseRepository<Layout>> mock;

        [SetUp]
        public void Init()
        {
            mock = new Mock<IBaseRepository<Layout>>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BLMappingProfile());
            });
            IMapper mapper = new Mapper(config);
            service = new LayoutService(mapper, mock.Object);
        }

        [TestCase(1)]
        public void GetLayoutTest(int id)
        {
            mock.Setup(a => a.Get(id)).Returns(new Layout() { Id = id });
            var idActual = service.Get(id).Id;
            Assert.AreEqual(id, idActual);
        }

        [TestCase(5)]
        public void GetLayoutNotExistTest(int id)
        {
            mock.Setup(a => a.Get(id)).Returns((Layout)null);

            Assert.Throws<NullReferenceException>(() => service.Get(id));
        }

        [Test]
        public void GetLayoutsTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Layout>().AsQueryable());

            service.GetAll();

            mock.Verify(verify => verify.GetAll());
        }

        [Test]
        public void CreateLayoutTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Layout>().AsQueryable());
            var layout = new LayoutDTO()
            {
                Description = "something description",
                Name = "name"
            };
            Assert.DoesNotThrow(() => service.Create(layout));
        }

        [Test]
        public void CreateLayoutNullTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Layout>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Create(null));
        }

        [Test]
        public void CreateLayoutNameUniqueTest()
        {
            var layoutDto = new LayoutDTO()
            {
                Description = "something description",
                Name = "name"
            };
            var layout = new Layout()
            {
                Description = "something description",
                Name = "name"
            };
            mock.Setup(a => a.GetAll()).Returns(new List<Layout>() { layout }.AsQueryable());
            Assert.That(
                () => { service.Create(layoutDto); },
                Throws.Exception
                    .TypeOf<UniqueArgumentException>()
                    .With.Property("Message")
                    .EqualTo("The name must be unique"));
        }

        [Test]
        public void UpdateLayoutTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Layout>().AsQueryable());
            var layout = new LayoutDTO()
            {
                Description = "something description",
                Name = "name"
            };
            Assert.DoesNotThrow(() => service.Update(layout));
        }

        [Test]
        public void UpdateLayoutNullTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Layout>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Update(null));
        }

        [Test]
        public void UpdateLayoutNameUniqueTest()
        {
            var layoutDto = new LayoutDTO()
            {
                Description = "something description",
                Name = "name"
            };
            var layout = new Layout()
            {
                Description = "something description",
                Name = "name"
            };
            mock.Setup(a => a.GetAll()).Returns(new List<Layout>() { layout }.AsQueryable());
            Assert.That(
                () => { service.Update(layoutDto); },
                Throws.Exception
                    .TypeOf<UniqueArgumentException>()
                    .With.Property("Message")
                    .EqualTo("The name must be unique"));
        }

        [TestCase(2)]
        public void DeleteLayout(int id)
        {
            mock.Setup(a => a.Get(id)).Returns((Layout)null);

            service.Delete(id);

            Assert.Throws<NullReferenceException>(() => service.Get(id));
        }
    }
}
