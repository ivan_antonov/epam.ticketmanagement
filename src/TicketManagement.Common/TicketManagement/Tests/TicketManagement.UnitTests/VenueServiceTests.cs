﻿namespace TicketManagement.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using BLL.DTO;
    using BLL.Infrastructure;
    using BLL.Interfaces;
    using BLL.Services;
    using DAL.Entities;
    using DAL.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class VenueServiceTests
    {
        private IVenueService service;
        private Mock<IBaseRepository<Venue>> mock;

        [SetUp]
        public void Init()
        {
            mock = new Mock<IBaseRepository<Venue>>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BLMappingProfile());
            });
            IMapper mapper = new Mapper(config);
            service = new VenueService(mapper, mock.Object);
        }

        [TestCase(1)]
        public void GetVenueTest(int id)
        {
            mock.Setup(a => a.Get(id)).Returns(new Venue() { Id = id });
            var idActual = service.Get(id).Id;
            Assert.AreEqual(id, idActual);
        }

        [TestCase(5)]
        public void GetVenueNotExistTest(int id)
        {
            mock.Setup(a => a.Get(id)).Returns((Venue)null);

            Assert.Throws<NullReferenceException>(() => service.Get(id));
        }

        [Test]
        public void GetVenuesTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Venue>().AsQueryable());

            service.GetAll();

            mock.Verify(verify => verify.GetAll());
        }

        [Test]
        public void CreateVenueTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Venue>().AsQueryable());
            var venue = new VenueDTO()
            {
                Address = "something address",
                Description = "something description",
                Name = "name",
                Phone = "9 12 1234"
            };
            Assert.DoesNotThrow(() => service.Create(venue));
        }

        [Test]
        public void CreateVenueNullTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Venue>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Create(null));
        }

        [Test]
        public void CreateVenueNameUniqueTest()
        {
            var venueDto = new VenueDTO()
            {
                Address = "something address",
                Description = "something description",
                Name = "name",
                Phone = "9 12 1234"
            };
            var venue = new Venue()
            {
                Address = "something address",
                Description = "something description",
                Name = "name",
                Phone = "9 12 1234"
            };
            mock.Setup(a => a.GetAll()).Returns(new List<Venue>() { venue }.AsQueryable());
            Assert.That(
                () => { service.Create(venueDto); },
                Throws.Exception
                    .TypeOf<UniqueArgumentException>()
                    .With.Property("Message")
                    .EqualTo("The name must be unique"));
        }

        [Test]
        public void UpdateVenueTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Venue>().AsQueryable());
            var venue = new VenueDTO()
            {
                Id = 1,
                Address = "something address",
                Description = "something description",
                Name = "name",
                Phone = "9 12 1234"
            };
            Assert.DoesNotThrow(() => service.Update(venue));
        }

        [Test]
        public void UpdateVenueNullTest()
        {
            mock.Setup(a => a.GetAll()).Returns(new List<Venue>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Update(null));
        }

        [Test]
        public void UpdateVenueNameUniqueTest()
        {
            var venueDto = new VenueDTO()
            {
                Id = 1,
                Address = "something address",
                Description = "something description",
                Name = "name",
                Phone = "9 12 1234"
            };
            var venue = new Venue()
            {
                Id = 1,
                Address = "something address is updated",
                Description = "something description",
                Name = "name",
                Phone = "9 12 1234"
            };
            mock.Setup(a => a.GetAll()).Returns(new List<Venue>() { venue }.AsQueryable());
            Assert.That(
                () => { service.Update(venueDto); },
                Throws.Exception
                    .TypeOf<UniqueArgumentException>()
                    .With.Property("Message")
                    .EqualTo("The name must be unique"));
        }

        [TestCase(2)]
        public void DeleteVenue(int id)
        {
            mock.Setup(a => a.Get(id)).Returns((Venue)null);

            service.Delete(id);

            Assert.Throws<NullReferenceException>(() => service.Get(id));
        }
    }
}
