﻿namespace TicketManagement.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using BLL.DTO;
    using BLL.Infrastructure;
    using BLL.Interfaces;
    using BLL.Services;
    using DAL.Entities;
    using DAL.Interfaces;
    using Moq;
    using NUnit.Framework;
    using BLL.Infrastructure.CustomException;

    [TestFixture]
    internal class EventServiceTests
    {
        private IEventService service;
        private Mock<IEventRepository> mockEvent;
        private Mock<IBaseRepository<Venue>> mockVenue;
        private Mock<IBaseRepository<Layout>> mockLayout;
        private Mock<IBaseRepository<Area>> mockArea;
        private Mock<IBaseRepository<Seat>> mockSeat;
        private Mock<IBaseRepository<EventArea>> mockEventArea;
        private Mock<IBaseRepository<EventSeat>> mockEventSeat;
        private Mock<IBaseRepository<BookSeat>> mockBookSeat;

        [SetUp]
        public void Init()
        {
            mockEvent = new Mock<IEventRepository>();
            mockVenue = new Mock<IBaseRepository<Venue>>();
            mockLayout = new Mock<IBaseRepository<Layout>>();
            mockArea = new Mock<IBaseRepository<Area>>();
            mockSeat = new Mock<IBaseRepository<Seat>>();
            mockEventArea = new Mock<IBaseRepository<EventArea>>();
            mockEventSeat = new Mock<IBaseRepository<EventSeat>>();
            mockBookSeat = new Mock<IBaseRepository<BookSeat>>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BLMappingProfile());
            });
            IMapper mapper = new Mapper(config);
            service = new EventService(mapper, mockEvent.Object, mockEventSeat.Object, mockEventArea.Object, mockLayout.Object, mockVenue.Object, mockArea.Object, mockSeat.Object, mockBookSeat.Object);
        }

        [Test]
        public void GetEventsTest()
        {
            mockEvent.Setup(a => a.GetAll()).Returns(new List<Event>().AsQueryable());

            service.GetAll();

            mockEvent.Verify(verify => verify.GetAll());
        }

        [Test]
        public void GetEventTest()
        {
            // Arrange
            mockEvent.Setup(db => db.Get(It.IsAny<int>()))
                .Returns(new Event());

            mockLayout.Setup(db => db.Get(It.IsAny<int>()))
                .Returns(new Layout());

            mockVenue.Setup(db => db.Get(It.IsAny<int>()))
                .Returns(new Venue());

            mockEventArea.Setup(db => db.Get(It.IsAny<int>()))
                .Returns(new EventArea());

            mockEventSeat.Setup(db => db.Get(It.IsAny<int>()))
                .Returns(new EventSeat());

            // Act
            service.Get(2);

            // Assert
            mockEvent.Verify(verify => verify.Get(It.IsAny<int>()));
        }

        [Test]
        public void GetEventNotExistTest()
        {
            mockEvent.Setup(a => a.Get(It.IsAny<int>())).Returns((Event)null);

            Assert.Throws<NullReferenceException>(() => service.Get(5));
        }

        [Test]
        public void CreateEventTest()
        {
            var area = new Area() { Id = 1, LayoutId = 1 };
            var seat = new Seat() { AreaId = 1 };
            mockEvent.Setup(a => a.GetAll()).Returns(new List<Event>().AsQueryable());
            mockArea.Setup(a => a.GetAll()).Returns(new List<Area>() { area }.AsQueryable());
            mockSeat.Setup(a => a.GetAll()).Returns(new List<Seat>() { seat }.AsQueryable());
            var item = new EventDTO()
            {
                Id = 1,
                Description = "something description",
                LayoutId = 1,
                Name = "something name",
                DateStart = DateTimeOffset.Now.AddHours(1),
                DateEnd = DateTimeOffset.Now.AddHours(2)
            };

            service.Create(item);

            mockEvent.Verify(verify => verify.GetAll());
            mockArea.Verify(verify => verify.GetAll());
            mockSeat.Verify(verify => verify.GetAll());
        }

        [Test]
        public void CreateEventNullTest()
        {
            mockEvent.Setup(a => a.GetAll()).Returns(new List<Event>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Create(null));
        }

        [Test]
        public void CreateEventInThePastTest()
        {
            mockEvent.Setup(a => a.GetAll()).Returns(new List<Event>().AsQueryable());
            var item = new EventDTO()
            {
                Id = 1,
                Description = "something description",
                LayoutId = 1,
                Name = "something name",
                DateStart = DateTimeOffset.Now.AddHours(-1),
                DateEnd = DateTimeOffset.Now.AddHours(2)
            };
            Assert.That(
                () => { service.Create(item); },
                Throws.Exception
                    .TypeOf<InvalidDateException>()
                    .With.Property("Message")
                    .EqualTo("Can not create event in the past"));
        }

        [Test]
        public void CreateEventInTheSameTimeTest()
        {
            var item = new Event()
            {
                Id = 1,
                Description = "something description",
                LayoutId = 1,
                Name = "something name",
                DateStart = DateTimeOffset.Now.AddHours(-1),
                DateEnd = DateTimeOffset.Now
            };
            var lastItem = new Event()
            {
                Id = 1,
                Description = "something description",
                LayoutId = 1,
                Name = "something name",
                DateStart = DateTimeOffset.Now.AddHours(1),
                DateEnd = DateTimeOffset.Now.AddHours(3)
            };
            mockEvent.Setup(a => a.GetAll()).Returns(new List<Event>() { item, lastItem }.AsQueryable());
            var itemDto = new EventDTO()
            {
                Id = 1,
                Description = "something description",
                LayoutId = 1,
                Name = "something name",
                DateStart = DateTimeOffset.Now.AddMinutes(5),
                DateEnd = DateTimeOffset.Now.AddHours(2)
            };
            Assert.That(
                () => { service.Create(itemDto); },
                Throws.Exception
                    .TypeOf<InvalidDateException>()
                    .With.Property("Message")
                    .EqualTo("The time is not vacant"));
        }

        [Test]
        public void CreateEventDatesIncorrectTest()
        {
            mockEvent.Setup(a => a.GetAll()).Returns(new List<Event>().AsQueryable());
            var item = new EventDTO()
            {
                Id = 1,
                Description = "something description",
                LayoutId = 1,
                Name = "something name",
                DateStart = DateTimeOffset.Now.AddHours(1),
                DateEnd = DateTimeOffset.Now.AddHours(-2)
            };
            Assert.That(
                () => { service.Create(item); },
                Throws.Exception
                    .TypeOf<InvalidDateException>()
                    .With.Property("Message")
                    .EqualTo("Dates are incorrect"));
        }

        [Test]
        public void UpdateEventTest()
        {
            var area = new Area() { Id = 1, LayoutId = 1 };
            var seat = new Seat() { AreaId = 1 };

            var itemEvent = new Event()
            {
                Id = 2,
                Description = "something description one",
                LayoutId = 1,
                Name = "something name one",
                DateStart = DateTimeOffset.Now.AddHours(8),
                DateEnd = DateTimeOffset.Now.AddHours(10)
            };

            mockEvent.Setup(a => a.GetAll()).Returns(new List<Event>().AsQueryable());
            mockEvent.Setup(a => a.Get(It.IsAny<int>())).Returns(itemEvent);
            mockArea.Setup(a => a.GetAll()).Returns(new List<Area>() { area }.AsQueryable());
            mockSeat.Setup(a => a.GetAll()).Returns(new List<Seat>() { seat }.AsQueryable());

            mockEventArea.Setup(a => a.GetAll()).Returns(new List<EventArea>().AsQueryable());
            mockEventSeat.Setup(a => a.GetAll()).Returns(new List<EventSeat>().AsQueryable());

            var item = new EventDTO()
            {
                Id = 1,
                Description = "something description",
                LayoutId = 1,
                Name = "something name",
                DateStart = DateTimeOffset.Now.AddHours(1),
                DateEnd = DateTimeOffset.Now.AddHours(3)
            };
            service.Update(item);
            mockEvent.Verify(verify => verify.GetAll());
            mockArea.Verify(verify => verify.GetAll());
            mockSeat.Verify(verify => verify.GetAll());
        }

        [Test]
        public void UpdateEventNullTest()
        {
            mockEvent.Setup(a => a.GetAll()).Returns(new List<Event>().AsQueryable());

            Assert.Throws<NullReferenceException>(() => service.Create(null));
        }

        [Test]
        public void DeleteEvent()
        {
            // Arrange
            mockEvent.Setup(a => a.Get(It.IsAny<int>()))
                .Returns((Event)null);

            // Act
            service.Delete(2);

            // Assert
            mockEvent.Verify(verify => verify.Delete(It.IsAny<int>()));
        }
    }
}
