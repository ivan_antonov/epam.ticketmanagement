﻿// <copyright file="LayoutDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Models
{
    using System.Runtime.Serialization;
    using System.Collections.Generic;

    [DataContract]
    public class LayoutDTO
    {

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int VenueId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public List<AreaDTO> Areas { get; set; }
    }
}
