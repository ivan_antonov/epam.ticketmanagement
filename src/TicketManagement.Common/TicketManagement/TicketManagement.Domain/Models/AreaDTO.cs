﻿// <copyright file="AreaDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Models
{

    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class AreaDTO
    {

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int LayoutId { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int CoordX { get; set; }

        [DataMember]
        public int CoordY { get; set; }

        [DataMember]
        public List<SeatDTO> Seats { get; set; }
    }
}