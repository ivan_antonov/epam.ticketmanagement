﻿// <copyright file="EventAreaDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Models
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class BookSeatDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int EventSeatId { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public DateTimeOffset Time { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public int Row { get; set; }

        [DataMember]
        public int Number { get; set; }
    }
}
