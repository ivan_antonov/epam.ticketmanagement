﻿// <copyright file="SeatDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Models
{
    using System.Runtime.Serialization;

    [DataContract]
    public class SeatDTO
    {

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int AreaId { get; set; }

        [DataMember]
        public int Row { get; set; }

        [DataMember]
        public int Number { get; set; }
    }
}