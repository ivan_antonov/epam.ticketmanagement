﻿// <copyright file="EventDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Models
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class EventDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int LayoutId { get; set; }

        [DataMember]
        public DateTimeOffset DateStart { get; set; }

        [DataMember]
        public DateTimeOffset DateEnd { get; set; }

        [DataMember]
        public string Poster { get; set; }

        [DataMember]
        public bool Visible { get; set; }

        [DataMember]
        public EventLayoutDTO Layout { get; set; }
    }
}