﻿// <copyright file="LayoutDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Models
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class EventLayoutDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int VenueId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public List<EventAreaDTO> Areas { get; set; }

        [DataMember]
        public VenueDTO Venue { get; set; }
    }
}
