﻿// <copyright file="VenueDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>


namespace TicketManagement.Domain.Models
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class EventPurchaseDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Poster { get; set; }

        [DataMember]
        public DateTimeOffset DatePurchase { get; set; }

        [DataMember]
        public decimal Price { get; set; }
    }
}
