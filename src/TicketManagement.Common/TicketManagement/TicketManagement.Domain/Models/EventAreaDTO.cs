﻿// <copyright file="EventAreaDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Models
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class EventAreaDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int EventId { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int CoordX { get; set; }

        [DataMember]
        public int CoordY { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public List<EventSeatDTO> Seats { get; set; }
    }
}