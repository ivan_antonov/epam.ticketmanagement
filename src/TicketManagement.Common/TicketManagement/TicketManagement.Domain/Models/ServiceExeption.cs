﻿using System.Runtime.Serialization;

namespace TicketManagement.Domain.Models
{
    [DataContract]
    public class ServiceExeption
    {
        [DataMember]
        public string Message { get; set; }
    }
}