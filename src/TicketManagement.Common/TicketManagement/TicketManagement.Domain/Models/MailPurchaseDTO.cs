﻿namespace TicketManagement.Domain.Models
{
    using System.Runtime.Serialization;

    [DataContract]
    public class MailPurchaseDTO
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public string Subject { get; set; }
    }
}