﻿using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace TicketManagement.Domain
{
    using System.IdentityModel.Selectors;
    using System.IdentityModel.Tokens;

    public class UserPasswordValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if (userName == "TicketManagement" && password == "TicketManagement")
            {
                return;
            }
            var reader = new AppSettingsReader();

            var urlValue = (string)reader.GetValue("UserApiUrl", typeof(string));
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", password);
            var responseMessage = httpClient.GetAsync(urlValue).Result;

            if (responseMessage.IsSuccessStatusCode)
            {
                return;
            }

            throw new SecurityTokenValidationException();
        }
    }
}