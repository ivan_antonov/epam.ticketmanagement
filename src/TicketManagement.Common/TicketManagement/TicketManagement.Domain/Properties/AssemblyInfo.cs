﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью указанного ниже 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("TicketManagement.Domain")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("TicketManagement.Domain")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Если для атрибута ComVisible задать значение FALSE, типы в этой сборке будут невидимыми 
// для компонентов COM. Если в этой сборке необходимо получить доступ к типу из 
// компонента COM, задайте для атрибута ComVisible этого типа значение TRUE.
[assembly: ComVisible(false)]

// Указанный ниже идентификатор GUID предназначен для идентификации библиотеки типов, если этот проект будет видимым для COM-объектов
[assembly: Guid("cc76296a-1add-43c3-ad8a-eb507d35b659")]

// Сведения о версии сборки состоят из указанных ниже четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер сборки
//      Номер редакции
//
// Можно указать все значения или установить для номеров редакции и сборки значение по умолчанию 
// с помощью символа "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
