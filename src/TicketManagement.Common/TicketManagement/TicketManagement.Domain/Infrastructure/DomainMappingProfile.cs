﻿namespace TicketManagement.Domain.Infrastructure
{
    using AutoMapper;
    using Models;

    public class DomainMappingProfile : Profile
    {
        public DomainMappingProfile()
        {
            CreateMap<BLL.DTO.EventDTO, EventDTO>().ReverseMap();
            CreateMap<BLL.DTO.EventAreaDTO, EventAreaDTO>().ReverseMap();
            CreateMap<BLL.DTO.EventLayoutDTO, EventLayoutDTO>().ReverseMap();
            CreateMap<BLL.DTO.LayoutDTO, LayoutDTO>().ReverseMap();
            CreateMap<BLL.DTO.LayoutDTO, LayoutDTO>().ReverseMap();
            CreateMap<BLL.DTO.EventPurchaseDTO, EventPurchaseDTO>().ReverseMap();
            CreateMap<BLL.DTO.BookSeatDTO, BookSeatDTO>().ReverseMap();
            CreateMap<BLL.DTO.VenueDTO, VenueDTO>().ReverseMap();
        }
    }
}