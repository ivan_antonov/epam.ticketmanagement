﻿namespace TicketManagement.Domain.Infrastructure
{
    using Ninject.Modules;
    using BLL.Interfaces;
    using BLL.Services;
    using AutoMapper;
    using Ninject;
    using BLL.Infrastructure;

    public class DomainModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IVenueService>().To<VenueService>();
            Bind<ILayoutService>().To<LayoutService>();
            Bind<IAreaService>().To<AreaService>();
            Bind<ISeatService>().To<SeatService>();
            Bind<IEventService>().To<EventService>();
            Bind<IBookSeatService>().To<BookSeatService>();
            Bind<IPurchaseService>().To<PurchaseService>();
            Bind<IUserService>().To<UserService>();
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BLMappingProfile());
                cfg.AddProfile(new DomainMappingProfile());
            });
            Bind<MapperConfiguration>().ToConstant(mapperConfiguration).InSingletonScope();
            Bind<IMapper>().ToMethod(ctx =>
                new Mapper(mapperConfiguration, type => ctx.Kernel.Get(type)));
        }
    }
}