﻿namespace TicketManagement.Domain.Interfaces
{
    using System.ServiceModel;
    using Models;

    [ServiceContract]
    public interface IMailWCFService
    {
        [OperationContract]
        void Send(MailPurchaseDTO model);
    }
}
