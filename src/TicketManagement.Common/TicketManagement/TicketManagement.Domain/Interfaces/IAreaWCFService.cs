﻿// <copyright file="IAreaService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Interfaces
{
    using System.Linq;
    using System.ServiceModel;
    using Models;

    [ServiceContract]
    public interface IAreaWCFService
    {
        [OperationContract]
        IQueryable<AreaDTO> GetAll();

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        AreaDTO Get(int id);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Create(AreaDTO area);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Update(AreaDTO area);

        [OperationContract]
        void Delete(int id);
    }
}