﻿// <copyright file="ISeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Interfaces
{
    using System.Linq;
    using System.ServiceModel;
    using Models;

    [ServiceContract]
    public interface IPurchaseWCFService
    {
        [OperationContract]
        IQueryable<EventPurchaseDTO> GetEventPurchasesById(string id);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void BuySeats(int eventId, string userId);
    }
}