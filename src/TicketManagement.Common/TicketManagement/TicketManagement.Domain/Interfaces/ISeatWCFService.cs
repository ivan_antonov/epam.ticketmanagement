﻿// <copyright file="ISeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Interfaces
{
    using System.Linq;
    using System.ServiceModel;
    using Models;

    [ServiceContract]
    public interface ISeatWCFService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        IQueryable<SeatDTO> GetAll();

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        SeatDTO Get(int id);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Create(SeatDTO seat);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Update(SeatDTO seat);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Delete(int id);
    }
}