﻿// <copyright file="IVenueService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Interfaces
{
    using System.Linq;
    using System.ServiceModel;
    using Models;

    [ServiceContract]
    public interface IVenueWCFService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        IQueryable<VenueDTO> GetAll();

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        VenueDTO Get(int id);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Create(VenueDTO venue);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Update(VenueDTO venue);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Delete(int id);
    }
}