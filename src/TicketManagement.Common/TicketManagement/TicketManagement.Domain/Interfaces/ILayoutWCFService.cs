﻿// <copyright file="ILayoutService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Interfaces
{
    using System.Linq;
    using System.ServiceModel;
    using Models;

    [ServiceContract]
    public interface ILayoutWCFService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        IQueryable<LayoutDTO> GetAll();

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        IQueryable<LayoutDTO> GetAllByVenueId(int id);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        LayoutDTO Get(int id);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Create(LayoutDTO layout);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Update(LayoutDTO layout);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Delete(int id);
    }
}