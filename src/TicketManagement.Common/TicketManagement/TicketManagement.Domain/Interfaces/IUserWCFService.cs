﻿using System.Linq;
using System.ServiceModel;
using TicketManagement.Domain.Models;

namespace TicketManagement.Domain.Interfaces
{
    [ServiceContract]
    public interface IUserWCFService
    {
        [OperationContract]
        IQueryable<LanguageDTO> GetLanguages();
    }
}
