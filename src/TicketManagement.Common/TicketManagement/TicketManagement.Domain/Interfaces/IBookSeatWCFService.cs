﻿// <copyright file="ISeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Interfaces
{
    using System.Linq;
    using System.ServiceModel;
    using Models;

    [ServiceContract]
    public interface IBookSeatWCFService
    {
        [OperationContract]
        IQueryable<BookSeatDTO> GetAll();

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        BookSeatDTO Get(int id);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Create(BookSeatDTO seat);

        [OperationContract]
        void Delete(int id);
    }
}