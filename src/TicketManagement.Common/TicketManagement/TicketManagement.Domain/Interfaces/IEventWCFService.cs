﻿// <copyright file="IEventService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Interfaces
{
    using System.Linq;
    using System.ServiceModel;
    using Models;

    [ServiceContract]
    public interface IEventWCFService
    {
        [OperationContract]
        IQueryable<EventDTO> GetAll();

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        EventDTO Get(int id);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        EventDTO GetByUser(int id, string user);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        IQueryable<EventAreaDTO> GetAllArea();

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Create(EventDTO eventDto);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Update(EventDTO eventDto);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void UpdateArea(EventAreaDTO eventDto);

        [OperationContract]
        [FaultContract(typeof(ServiceExeption))]
        void Delete(int id);
    }
}