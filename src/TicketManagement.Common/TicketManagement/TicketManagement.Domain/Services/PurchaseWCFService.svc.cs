﻿// <copyright file="PurchaseWCFService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using BLL.Infrastructure.CustomException;
    using AutoMapper;
    using Interfaces;
    using Models;

    public class PurchaseWCFService : IPurchaseWCFService
    {
        private readonly BLL.Interfaces.IPurchaseService purchaseService;
        private readonly IMapper mapper;

        public PurchaseWCFService(IMapper mapper, BLL.Interfaces.IPurchaseService purchaseService)
        {
            this.mapper = mapper;
            this.purchaseService = purchaseService;
        }

        public IQueryable<EventPurchaseDTO> GetEventPurchasesById(string id)
        {
            return mapper.Map<IQueryable<BLL.DTO.EventPurchaseDTO>, List<EventPurchaseDTO>>(purchaseService.GetEventPurchasesById(id)).AsQueryable();
        }

        public void BuySeats(int eventId, string userId)
        {
            try
            {
                purchaseService.BuySeats(eventId, userId);
            }
            catch (NotEnoughFundsException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NotEnoughFundsException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }
    }
}
