﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using TicketManagement.BLL.Interfaces;
using TicketManagement.Domain.Interfaces;
using TicketManagement.Domain.Models;

namespace TicketManagement.Domain.Services
{
    public class UserWCFService : IUserWCFService
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UserWCFService(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        public IQueryable<LanguageDTO> GetLanguages()
        {
            var languages = userService.GetLanguages();
            return mapper.Map<IQueryable<BLL.DTO.LanguageDTO>, List<LanguageDTO>>(languages).AsQueryable();
        }
    }
}
