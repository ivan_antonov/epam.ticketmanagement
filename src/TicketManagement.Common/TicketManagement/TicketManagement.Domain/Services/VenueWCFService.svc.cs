﻿// <copyright file="VenueWCFService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using System.ServiceModel;
    using AutoMapper;
    using Interfaces;
    using Models;
    using BLL.Infrastructure;

    public class VenueWCFService : IVenueWCFService
    {
        private readonly BLL.Interfaces.IVenueService venueService;
        private readonly IMapper mapper;

        public VenueWCFService(BLL.Interfaces.IVenueService venueService, IMapper mapper)
        {
            this.venueService = venueService;
            this.mapper = mapper;
        }

        public IQueryable<VenueDTO> GetAll()
        {
            try
            {
                return mapper.Map<IQueryable<BLL.DTO.VenueDTO>, List<VenueDTO>>(venueService.GetAll()).AsQueryable();
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }

        }

        public VenueDTO Get(int id)
        {
            try
            {
                return mapper.Map<BLL.DTO.VenueDTO, VenueDTO>(venueService.Get(id));
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Create(VenueDTO venue)
        {
            try
            {
                var venueBll = mapper.Map<VenueDTO, BLL.DTO.VenueDTO>(venue);
                venueService.Create(venueBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
            catch (UniqueArgumentException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "UniqueArgumentException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Update(VenueDTO venue)
        {
            try
            {
                var venueBll = mapper.Map<VenueDTO, BLL.DTO.VenueDTO>(venue);
                venueService.Update(venueBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError);
            }
            catch (UniqueArgumentException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "UniqueArgumentException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Delete(int id)
        {
            try
            {
                venueService.Delete(id);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }
    }
}
