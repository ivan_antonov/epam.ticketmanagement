﻿// <copyright file="LayoutService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Interfaces;
    using Models;
    using System;
    using System.ServiceModel;
    using BLL.Infrastructure;

    public class LayoutWCFService : ILayoutWCFService
    {
        private readonly BLL.Interfaces.ILayoutService layoutService;
        private readonly IMapper mapper;

        public LayoutWCFService(BLL.Interfaces.ILayoutService layoutService, IMapper mapper)
        {
            this.layoutService = layoutService;
            this.mapper = mapper;
        }

        public IQueryable<LayoutDTO> GetAll()
        {
            try
            {
                return mapper.Map<IQueryable<BLL.DTO.LayoutDTO>, List<LayoutDTO>>(layoutService.GetAll()).AsQueryable();
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public IQueryable<LayoutDTO> GetAllByVenueId(int id)
        {
            try
            {
                return mapper.Map<IQueryable<BLL.DTO.LayoutDTO>, List<LayoutDTO>>(layoutService.GetAllByVenueId(id)).AsQueryable();
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public LayoutDTO Get(int id)
        {
            try
            {
                return mapper.Map<BLL.DTO.LayoutDTO, LayoutDTO>(layoutService.Get(id));
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Create(LayoutDTO layout)
        {
            try
            {
                var layoutBll = mapper.Map<LayoutDTO, BLL.DTO.LayoutDTO>(layout);
                layoutService.Create(layoutBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
            catch (UniqueArgumentException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "UniqueArgumentException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Update(LayoutDTO layout)
        {
            try
            {
                var layoutBll = mapper.Map<LayoutDTO, BLL.DTO.LayoutDTO>(layout);
                layoutService.Update(layoutBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
            catch (UniqueArgumentException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "UniqueArgumentException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Delete(int id)
        {
            try
            {
                layoutService.Delete(id);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }
    }
}
