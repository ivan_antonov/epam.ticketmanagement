﻿// <copyright file="SeatWCFService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

using System;
using System.ServiceModel;
using TicketManagement.BLL.Infrastructure;

namespace TicketManagement.Domain.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Interfaces;
    using Models;

    public class SeatWCFService : ISeatWCFService
    {
        private readonly BLL.Interfaces.ISeatService seatService;
        private readonly IMapper mapper;

        public SeatWCFService(BLL.Interfaces.ISeatService seatService, IMapper mapper)
        {
            try
            {
                this.seatService = seatService;
                this.mapper = mapper;
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public IQueryable<SeatDTO> GetAll()
        {
            try
            {
                return mapper.Map<IQueryable<BLL.DTO.SeatDTO>, List<SeatDTO>>(seatService.GetAll()).AsQueryable();
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public SeatDTO Get(int id)
        {
            try
            {
                return mapper.Map<BLL.DTO.SeatDTO, SeatDTO>(seatService.Get(id));
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Create(SeatDTO seat)
        {
            try
            {
                var seatBll = mapper.Map<SeatDTO, BLL.DTO.SeatDTO>(seat);
                seatService.Create(seatBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
            catch (UniqueArgumentException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "UniqueArgumentException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Update(SeatDTO seat)
        {
            try
            {
                var seatBll = mapper.Map<SeatDTO, BLL.DTO.SeatDTO>(seat);
                seatService.Update(seatBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
            catch (UniqueArgumentException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "UniqueArgumentException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Delete(int id)
        {
            try
            {
                seatService.Delete(id);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }
    }
}
