﻿// <copyright file="EventWCFService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

using System;
using System.ServiceModel;
using TicketManagement.BLL.Infrastructure.CustomException;

namespace TicketManagement.Domain.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Interfaces;
    using Models;

    public class EventWCFService : IEventWCFService
    {
        private readonly BLL.Interfaces.IEventService eventService;
        private readonly IMapper mapper;

        public EventWCFService(BLL.Interfaces.IEventService eventService, IMapper mapper)
        {
            this.eventService = eventService;
            this.mapper = mapper;
        }

        public IQueryable<EventDTO> GetAll()
        {
            return mapper.Map<IQueryable<BLL.DTO.EventDTO>, List<EventDTO>>(eventService.GetAll()).AsQueryable();
        }

        public EventDTO Get(int id)
        {
            try
            {
                return mapper.Map<BLL.DTO.EventDTO, EventDTO>(eventService.Get(id));
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }

        }

        public EventDTO GetByUser(int id, string user)
        {
            try
            {
                return mapper.Map<BLL.DTO.EventDTO, EventDTO>(eventService.Get(id));
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public IQueryable<EventAreaDTO> GetAllArea()
        {
            return mapper.Map<IQueryable<BLL.DTO.EventAreaDTO>, List<EventAreaDTO>>(eventService.GetAllArea()).AsQueryable();
        }

        public void Create(EventDTO eventDto)
        {
            try
            {
                var eventDtoBll = mapper.Map<EventDTO, BLL.DTO.EventDTO>(eventDto);
                eventService.Create(eventDtoBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
            catch (InvalidDateException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "InvalidDateException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
            catch (Exception)
            {
                var srvError = new ServiceExeption
                {
                    Message = "ExceptionAnySeats"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Update(EventDTO eventDto)
        {
            try
            {
                var eventDtoBll = mapper.Map<EventDTO, BLL.DTO.EventDTO>(eventDto);
                eventService.Update(eventDtoBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void UpdateArea(EventAreaDTO eventDto)
        {
            try
            {
                var eventDtoBll = mapper.Map<EventAreaDTO, BLL.DTO.EventAreaDTO>(eventDto);
                eventService.UpdateArea(eventDtoBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Delete(int id)
        {
            try
            {
                eventService.Delete(id);
            }
            catch (SeatBusyException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "SeatBusyException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }
    }
}
