﻿using System.Net.Mail;
using TicketManagement.Domain.Interfaces;
using TicketManagement.Domain.Models;

namespace TicketManagement.Domain.Services
{
    public class MailWCFService : IMailWCFService
    {
        public void Send(MailPurchaseDTO model)
        {
            var m = new MailMessage
            {
                Subject = model.Subject,
                Body = model.Text,
                IsBodyHtml = true
            };
            m.To.Add(model.Email);
            var smtp = new SmtpClient();

            smtp.Send(m);
        }
    }
}
