﻿// <copyright file="AreaWCFService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.Domain.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Interfaces;
    using Models;
    using System;
    using System.ServiceModel;
    using BLL.Infrastructure;

    public class AreaWCFService : IAreaWCFService
    {
        private readonly BLL.Interfaces.IAreaService areaService;
        private readonly IMapper mapper;

        public AreaWCFService(BLL.Interfaces.IAreaService areaService, IMapper mapper)
        {
            this.areaService = areaService;
            this.mapper = mapper;
        }

        public IQueryable<AreaDTO> GetAll()
        {
            return mapper.Map<IQueryable<BLL.DTO.AreaDTO>, List<AreaDTO>>(areaService.GetAll()).AsQueryable();
        }

        public AreaDTO Get(int id)
        {
            try
            {
                return mapper.Map<BLL.DTO.AreaDTO, AreaDTO>(areaService.Get(id));
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Create(AreaDTO area)
        {
            try
            {
                var areaBll = mapper.Map<AreaDTO, BLL.DTO.AreaDTO>(area);
                areaService.Create(areaBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError);
            }
            catch (UniqueArgumentException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "UniqueArgumentException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Update(AreaDTO area)
        {
            try
            {
                var areaBll = mapper.Map<AreaDTO, BLL.DTO.AreaDTO>(area);
                areaService.Update(areaBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
            catch (UniqueArgumentException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "UniqueArgumentException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Delete(int id)
        {
            areaService.Delete(id);
        }
    }
}
