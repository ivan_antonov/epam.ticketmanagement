﻿// <copyright file="BookSeatWCFService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>


namespace TicketManagement.Domain.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Interfaces;
    using Models;
    using System;
    using System.ServiceModel;

    public class BookSeatWCFService : IBookSeatWCFService
    {
        private readonly BLL.Interfaces.IBookSeatService bookSeatService;
        private readonly IMapper mapper;

        public BookSeatWCFService(BLL.Interfaces.IBookSeatService bookSeatService, IMapper mapper)
        {
            this.bookSeatService = bookSeatService;
            this.mapper = mapper;
        }

        public IQueryable<BookSeatDTO> GetAll()
        {
            return mapper.Map<IQueryable<BLL.DTO.BookSeatDTO>, List<BookSeatDTO>>(bookSeatService.GetAll()).AsQueryable();
        }

        public BookSeatDTO Get(int id)
        {
            try
            {
                return mapper.Map<BLL.DTO.BookSeatDTO, BookSeatDTO>(bookSeatService.Get(id));
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Create(BookSeatDTO seat)
        {
            try
            {
                var bookSeatBll = mapper.Map<BookSeatDTO, BLL.DTO.BookSeatDTO>(seat);
                bookSeatService.Create(bookSeatBll);
            }
            catch (NullReferenceException)
            {
                var srvError = new ServiceExeption
                {
                    Message = "NullReferenceException"
                };
                throw new FaultException<ServiceExeption>(srvError, new FaultReason(srvError.Message));
            }
        }

        public void Delete(int id)
        {
            bookSeatService.Delete(id);
        }
    }
}
