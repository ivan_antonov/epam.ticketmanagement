﻿namespace TicketManagement.IntegrationTests
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using AutoMapper;
    using BLL.DTO;
    using BLL.Infrastructure;
    using BLL.Interfaces;
    using BLL.Services;
    using DAL.EF;
    using DAL.Entities;
    using DAL.Interfaces;
    using DAL.Repositories;
    using NUnit.Framework;

    [TestFixture]
    public class EventManagementTests
    {
        private TicketManagementContext context;
        private IMapper mapper;
        private string connectionString;
        private TestData testData;
        private string path;
        private IEventRepository eventRepository;
        private IBaseRepository<EventArea> eventAreaRepository;
        private IBaseRepository<EventSeat> eventSeatRepository;
        private IBaseRepository<BookSeat> bookSeatRepository;
        private IBaseRepository<Venue> venueRepository;
        private IBaseRepository<Layout> layoutRepository;
        private IBaseRepository<Area> areaRepository;
        private IBaseRepository<Seat> seatRepository;
        private IBaseRepository<Purchase> purchaseRepository;
        private IUserRepository userRepository;
        private IBaseRepository<UserRole> userRoleRepository;
        private IBaseRepository<Role> roleRepository;

        [SetUp]
        public void Init()
        {
            connectionString = ConfigurationManager.ConnectionStrings["test"].ConnectionString;
            var location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            path = Path.GetDirectoryName(location) + "\\Scripts\\";
            SQLHelper.ExecScript(connectionString, $"{path}SnapshotScript.sql");
            testData = new TestData();
            context = new TicketManagementContext("test");
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BLMappingProfile());
            });
            mapper = new Mapper(config);
            eventRepository = new EventRepository(context);
            eventAreaRepository = new EFRepository<EventArea>(context);
            eventSeatRepository = new EFRepository<EventSeat>(context);
            bookSeatRepository = new EFRepository<BookSeat>(context);
            venueRepository = new EFRepository<Venue>(context);
            layoutRepository = new EFRepository<Layout>(context);
            areaRepository = new EFRepository<Area>(context);
            seatRepository = new EFRepository<Seat>(context);
            purchaseRepository = new EFRepository<Purchase>(context);
            userRepository = new UserRepository(context);
            userRoleRepository = new EFRepository<UserRole>(context);
            roleRepository = new EFRepository<Role>(context);
        }

        [TearDown]
        public void Finish()
        {
            SQLHelper.ExecScript(connectionString, $"{path}RestoreDBScript.sql");
            context.Dispose();
        }

        [Test]
        public void CreateEventIntegration()
        {
            IEventService eventService = new EventService(mapper, eventRepository, eventSeatRepository, eventAreaRepository, layoutRepository, venueRepository, areaRepository, seatRepository, bookSeatRepository);
            var events = eventService.GetAll().Count();
            ILayoutService layoutService = new LayoutService(mapper, layoutRepository);
            var layout = layoutService.GetAll().FirstOrDefault();
            if (layout != null)
            {
                var testEvent = testData.EventDto;
                testEvent.LayoutId = layout.Id;

                eventService.Create(testEvent);

                Assert.AreNotEqual(events, eventService.GetAll().Count());
            }
        }

        [Test]
        public void UpdateEventIntegration()
        {
            IEventService eventService = new EventService(mapper, eventRepository, eventSeatRepository, eventAreaRepository, layoutRepository, venueRepository, areaRepository, seatRepository, bookSeatRepository);
            var newEvent = eventService.Get(1);

            newEvent.Name = "new Name";
            Assert.DoesNotThrow(() => eventService.Update(newEvent));

        }

        [Test]
        public void DeleteEventIntegration()
        {
            IEventService eventService = new EventService(mapper, eventRepository, eventSeatRepository, eventAreaRepository, layoutRepository, venueRepository, areaRepository, seatRepository, bookSeatRepository);
            ILayoutService layoutService = new LayoutService(mapper, layoutRepository);
            var layout = layoutService.Get(1);
            if (layout != null)
            {
                var testEvent = testData.EventDto;
                testEvent.DateStart = DateTimeOffset.Now.AddYears(12).AddHours(1);
                testEvent.DateEnd = DateTimeOffset.Now.AddYears(12).AddHours(3);
                testEvent.LayoutId = layout.Id;

                eventService.Create(testEvent);
            }

            var actualEvent = eventService.GetAll().ToList();
            Assert.DoesNotThrow(() => eventService.Delete(actualEvent.Last().Id));
        }

        [Test]
        public void CreateVenueIntegration()
        {
            IVenueService venueService = new VenueService(mapper, venueRepository);
            var venue = venueService.GetAll().Count();
            venueService.Create(testData.Venue);

            Assert.AreNotEqual(venue, venueService.GetAll().Count());
        }

        [Test]
        public void CreateLayoutsIntegration()
        {
            IVenueService venueService = new VenueService(mapper, venueRepository);
            var venue = venueService.GetAll().FirstOrDefault();
            var testLayout = testData.HistoricalLayout;
            testLayout.VenueId = venue.Id;
            ILayoutService layoutService = new LayoutService(mapper, layoutRepository);
            var layoutCount = layoutService.GetAll().Count();
            layoutService.Create(testLayout);

            Assert.AreNotEqual(layoutCount, layoutService.GetAll().Count());
        }

        [Test]
        public void CreateAreas()
        {
            ILayoutService layoutService = new LayoutService(mapper, layoutRepository);
            var layout = layoutService.GetAll().FirstOrDefault();
            var testArea = testData.BalconyArea;
            testArea.LayoutId = layout.Id;
            IAreaService areaService = new AreaService(mapper, areaRepository);
            var areaCount = areaService.GetAll().Count();
            areaService.Create(testArea);

            Assert.AreNotEqual(areaCount, areaService.GetAll().Count());
        }

        [Test]
        public void CreateSeatsIntegration()
        {
            IAreaService areaService = new AreaService(mapper, areaRepository);
            var area = areaService.GetAll().FirstOrDefault();
            var testSeat = new SeatDTO
            {
                AreaId = area.Id,
                Number = 10,
                Row = 100
            };

            ISeatService seatService = new SeatService(mapper, seatRepository);
            var seatCount = seatService.GetAll().Count();
            seatService.Create(testSeat);

            Assert.AreNotEqual(seatCount, seatService.GetAll().Count());
        }
    }
}
