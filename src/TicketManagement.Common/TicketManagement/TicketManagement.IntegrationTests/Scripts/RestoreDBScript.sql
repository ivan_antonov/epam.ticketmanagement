﻿USE master;

DECLARE @DatabaseName nvarchar(50)
SET @DatabaseName = N'TicketManagement'

DECLARE @SQL varchar(max)

SELECT @SQL = COALESCE(@SQL,'') + ' BEGIN TRY Kill ' + Convert(varchar, SPId) + '; END TRY BEGIN CATCH END CATCH;'
FROM MASTER..SysProcesses
WHERE DBId = DB_ID(@DatabaseName) AND SPId <> @@SPId

EXEC(@SQL)

RESTORE DATABASE TicketManagement from 
DATABASE_SNAPSHOT = 'TicketManagementSnapshot';
GO

IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'TicketManagementSnapshot')
DROP DATABASE TicketManagementSnapshot
GO