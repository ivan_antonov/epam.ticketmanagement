﻿USE [master]
GO

IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'TicketManagementSnapshot')
DROP DATABASE TicketManagementSnapshot
GO

CREATE DATABASE TicketManagementSnapshot
ON (NAME = 'TicketManagement', FILENAME = 'D:\TicketManagement_Test.SNP')
   AS SNAPSHOT OF TicketManagement;