﻿namespace TicketManagement.IntegrationTests
{
    using System.Data.SqlClient;
    using System.IO;
    using Microsoft.SqlServer.Management.Common;
    using Microsoft.SqlServer.Management.Smo;

    internal static class SQLHelper
    {
        public static void ExecScript(string connectionString, string scriptName)
        {            
            var file = new FileInfo(Path.Combine("SQL", scriptName));
            string script = file.OpenText().ReadToEnd();
            var conn = new SqlConnection(connectionString);
            var server = new Server(new ServerConnection(conn));
            server.ConnectionContext.ExecuteNonQuery(script);
        }
    }
}