﻿namespace TicketManagement.IntegrationTests
{
    using System;
    using System.Collections.Generic;
    using BLL.DTO;

    class TestData
    {
        public VenueDTO Venue { get; set; } = new VenueDTO()
        {
            Name = "The Regent Street Cinema",
            Address = "309 Regent St, London W1B 2UW, BR",
            Description =
                "We are the Regent Street Cinema, a uniquely historic cinema",
            Phone = "0207 911 5050",
            Layouts = new List<LayoutDTO>()
        };

        public LayoutDTO ModernLayout { get; set; } = new LayoutDTO()
        {
            Name = "Modern hall",
            Description = "We are a modern look at historical events",
            Areas = new List<AreaDTO>()
        };

        public LayoutDTO HistoricalLayout { get; set; } = new LayoutDTO()
        {
            Name = "Historical hall",
            Description = "We show a movie on historical topics",
            Areas = new List<AreaDTO>()
        };

        public AreaDTO CentralArea { get; set; } = new AreaDTO()
        {
            CoordX = 0,
            CoordY = 100,
            Description = "Central area",
            Seats = new List<SeatDTO>()
        };

        public AreaDTO BalconyArea { get; set; } = new AreaDTO()
        {
            CoordX = 0,
            CoordY = 0,
            Description = "Balcony area",
            Seats = new List<SeatDTO>()
        };

        public EventDTO EventDto { get; set; } = new EventDTO()
        {
            Name = "Altera moderatid",
            Description = "Te tritani theophrastus mei, pri at suas timeam integre",
            DateStart = DateTimeOffset.Now.AddYears(1).AddHours(12),
            DateEnd = DateTimeOffset.Now.AddYears(1).AddHours(16),
            Poster = "somethingURL",
            Visible = false
        };
    }
}
