﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using TicketManagement.UserAPI.Models;

namespace TicketManagement.UserAPI.Core
{
    public class AuthRepository
    {
        private UserManager<UserModel> _userManager;

        public AuthRepository(UserManager<UserModel> userManager)
        {
            _userManager = userManager;
        }


        public async Task<IdentityResult> RegisterUser(UserAuthModel userModel)
        {
            var user = new UserModel
            {
                UserName = userModel.Login
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);
            return result;
        }

        public async Task<UserModel> FindUser(string userName, string password)
        {
            var user = await _userManager.FindAsync(userName, password);
            return user;
        }
    }
}