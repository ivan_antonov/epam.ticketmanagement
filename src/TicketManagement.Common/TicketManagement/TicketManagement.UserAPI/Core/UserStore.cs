﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.Identity;
using TicketManagement.BLL.DTO;
using TicketManagement.BLL.Interfaces;

namespace TicketManagement.UserAPI.Core
{
    public class UserStore : IUserStore<UserModel>, IUserPasswordStore<UserModel>
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UserStore(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        public void Dispose()
        {

        }

        public Task CreateAsync(UserModel user)
        {
            var userDto = mapper.Map<UserModel, UserDTO>(user);
            userService.Create(userDto);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(UserModel user)
        {
            var userDto = mapper.Map<UserModel, UserDTO>(user);
            userService.Update(userDto);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(UserModel user)
        {
            userService.Delete(user.Id);
            return Task.CompletedTask;
        }

        public Task<UserModel> FindByIdAsync(string userId)
        {
            var userDto = userService.GetUserById(userId);
            var userModel = mapper.Map<UserDTO, UserModel>(userDto);
            return Task.FromResult(userModel);
        }

        public Task<UserModel> FindByNameAsync(string userName)
        {
            var userDto = userService.GetUserByUserName(userName);
            var userModel = mapper.Map<UserDTO, UserModel>(userDto);
            return Task.FromResult(userModel);
        }

        public Task SetPasswordHashAsync(UserModel user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(UserModel user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(UserModel user)
        {
            return Task.FromResult(true);
        }
    }
}