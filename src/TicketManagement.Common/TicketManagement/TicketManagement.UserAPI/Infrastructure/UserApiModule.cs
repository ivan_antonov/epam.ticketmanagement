﻿// <copyright file="ServiceModule.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.UserAPI.Infrastructure
{
    using Microsoft.AspNet.Identity;
    using Ninject.Modules;
    using AutoMapper;
    using Ninject;
    using BLL.Infrastructure;
    using BLL.Interfaces;
    using BLL.Services;
    using Core;

    public class UserModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>();
            Bind<AuthRepository>().ToSelf();

            Bind<IUserStore<UserModel>>().To<UserStore>();
            Bind<UserManager<UserModel>>().ToSelf();

            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BLMappingProfile());
                cfg.AddProfile(new ApiMappingProfile());
            });
            Bind<MapperConfiguration>().ToConstant(mapperConfiguration).InSingletonScope();
            Bind<IMapper>().ToMethod(ctx =>
                new Mapper(mapperConfiguration, type => ctx.Kernel.Get(type)));
        }
    }
}
