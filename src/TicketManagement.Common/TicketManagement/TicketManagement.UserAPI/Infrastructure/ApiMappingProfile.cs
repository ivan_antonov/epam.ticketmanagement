﻿namespace TicketManagement.UserAPI.Infrastructure
{
    using AutoMapper;
    using BLL.DTO;
    using Core;
    using Models;

    public class ApiMappingProfile : Profile
    {
        public ApiMappingProfile()
        {
            CreateMap<UserDTO, UserModel>().ReverseMap();
            CreateMap<UserDTO, User>().ReverseMap();
        }
    }
}