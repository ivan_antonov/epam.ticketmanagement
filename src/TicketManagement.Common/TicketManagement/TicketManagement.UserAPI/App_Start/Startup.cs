﻿using System.Reflection;
using AutoMapper;
using Microsoft.Owin;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using TicketManagement.UserAPI.Infrastructure;

//using Ninject.Web.Common.OwinHost;
//using Ninject.Web.WebApi.OwinHost;

[assembly: OwinStartup(typeof(TicketManagement.UserAPI.Startup))]

namespace TicketManagement.UserAPI
{
    using System;
    using System.Web.Http;
    using Microsoft.Owin.Security.OAuth;
    using Ninject;
    using Ninject.Modules;
    using Owin;
    using BLL.Infrastructure;
    using Core;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            NinjectModule serviceModule = new ServiceModule("DefaultConnection");
            var kernel = new StandardKernel(serviceModule);
            kernel.Load(Assembly.GetExecutingAssembly());

            ConfigureOAuth(app, kernel);
            var config = new HttpConfiguration();
            SwaggerConfig.Register(config);
            WebApiConfig.Register(config);

            app.UseNinjectMiddleware(() => kernel)
                .UseNinjectWebApi(config);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void ConfigureOAuth(IAppBuilder app, StandardKernel kernel)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AuthorizationServerProvider(kernel.Get<AuthRepository>())
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}