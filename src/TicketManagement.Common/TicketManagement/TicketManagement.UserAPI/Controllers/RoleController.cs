﻿namespace TicketManagement.UserAPI.Controllers
{
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Swashbuckle.Swagger.Annotations;
    using BLL.Interfaces;

    public class RoleController : ApiController
    {
        private readonly IUserService userService;

        public RoleController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// Get roles by username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.NoContent, Type = typeof(void))]
        public async Task<IHttpActionResult> GetRoleByUserName(string userName)
        {
            var roles = userService.GetRoles(userName);
            if (roles is null)
            {
                return await Task.FromResult(BadRequest());
            }
            return await Task.FromResult(Ok(roles));
        }
    }
}
