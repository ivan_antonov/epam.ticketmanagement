﻿namespace TicketManagement.UserAPI.Controllers
{
    using System.Web.Http;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http.Results;
    using Swashbuckle.Swagger.Annotations;
    using BLL.DTO;
    using Models;
    using AutoMapper;
    using BLL.Interfaces;

    //[Authorize]
    public class UserController : ApiController
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Adds new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.NoContent, Type = typeof(void))]
        public async Task<IHttpActionResult> CreateUser([FromBody]User user)
        {
            if (user is null)
            {
                return await Task.FromResult(BadRequest());
            }

            var userDto = mapper.Map<User, UserDTO>(user);
            userService.Create(userDto);
            return new StatusCodeResult(HttpStatusCode.NoContent, Request);
        }

        /// <summary>
        /// Delete the user by id
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpDelete]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.NoContent, Type = typeof(void))]
        public IHttpActionResult DeleteUser(string id)
        {
            userService.Delete(id);
            return new StatusCodeResult(HttpStatusCode.NoContent, Request);
        }

        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.NoContent, Type = typeof(void))]
        public async Task<IHttpActionResult> GetUserById(string id)
        {
            var userDto = userService.GetUserById(id);
            if (userDto is null)
            {
                return await Task.FromResult(BadRequest());
            }

            var user = mapper.Map<UserDTO, User>(userDto);
            return await Task.FromResult(Ok(user));
        }

        /// <summary>
        /// Get user by username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.NoContent, Type = typeof(void))]
        public async Task<IHttpActionResult> GetUserByUserName(string userName)
        {
            var userDto = userService.GetUserByUserName(userName);
            if (userDto is null)
            {
                return await Task.FromResult(BadRequest());
            }

            var user = mapper.Map<UserDTO, User>(userDto);
            return await Task.FromResult(Ok(user));
        }

        /// <summary>
        /// Update the user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.NoContent, Type = typeof(void))]
        public async Task<IHttpActionResult> UpdateUser([FromBody]User user)
        {
            if (user is null)
            {
                return await Task.FromResult(BadRequest());
            }

            var userDto = mapper.Map<User, UserDTO>(user);
            userService.Update(userDto);
            return new StatusCodeResult(HttpStatusCode.NoContent, Request);
        }
    }
}
