﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Results;

namespace TicketManagement.UserAPI.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {

        [Authorize]
        [HttpGet]
        public IHttpActionResult Get()
        {
            return new StatusCodeResult(HttpStatusCode.OK, Request);
        }

    }
}
