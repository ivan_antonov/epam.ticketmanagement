﻿// <copyright file="Venue.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Venue
    /// </summary>
    [Table("Venue")]
    public class Venue
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        [Required]
        [MaxLength(120)]
        [Column("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        [Required]
        [MaxLength(120)]
        [Column("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets  address
        /// </summary>
        [Required]
        [MaxLength(200)]
        [Column("Address")]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets phone
        /// </summary>
        [Required]
        [MaxLength(30)]
        [Column("Phone")]
        public string Phone { get; set; }
    }
}
