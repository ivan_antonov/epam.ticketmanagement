﻿namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// UserRole
    /// </summary>
    [Table("UserRole")]
    public class UserRole
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Foreign key Users
        /// </summary>
        [Required]
        [Column("UserId")]
        public string UserId { get; set; }

        /// <summary>
        /// Foreign key Roles
        /// </summary>
        [Required]
        [Column("RoleId")]
        public int RoleId { get; set; }
    }
}
