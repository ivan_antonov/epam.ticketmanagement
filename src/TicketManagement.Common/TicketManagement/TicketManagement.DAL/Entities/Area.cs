﻿// <copyright file="Area.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Area
    /// </summary>
    [Table("Area")]
    public class Area
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Id of layout
        /// </summary>
        [Required]
        [Column("LayoutId")]
        public int LayoutId { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        [Required]
        [MaxLength(200)]
        [Column("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets CoordX
        /// </summary>
        [Required]
        [Column("CoordX")]
        public int CoordX { get; set; }

        /// <summary>
        /// Gets or sets CoordY
        /// </summary>
        [Required]
        [Column("CoordY")]
        public int CoordY { get; set; }
    }
}