﻿// <copyright file="EventArea.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// EventArea
    /// </summary>
    [Table("EventArea")]
    public class EventArea
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets id of event
        /// </summary>
        [Required]
        [Column("EventId")]
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        [Required]
        [MaxLength(200)]
        [Column("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets coordinate x
        /// </summary>
        [Required]
        [Column("CoordX")]
        public int CoordX { get; set; }

        /// <summary>
        /// Gets or sets coordinate y
        /// </summary>
        [Required]
        [Column("CoordY")]
        public int CoordY { get; set; }

        /// <summary>
        /// Gets or sets price
        /// </summary>
        [Required]
        [Column("Price")]
        public decimal Price { get; set; }
    }
}