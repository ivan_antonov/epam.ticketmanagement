﻿namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Users")]
    public class User
    {
        [Key]
        [Column("Id")]
        [MaxLength(50)]
        public string Id { get; set; }

        [Required]
        [Column("UserName")]
        [MaxLength(10)]
        public string UserName { get; set; }

        [Required]
        [Column("Email")]
        [MaxLength(30)]
        public string Email { get; set; }

        [Required]
        [Column("PasswordHash")]
        public string PasswordHash { get; set; }

        [Required]
        [Column("FirstName")]
        [MaxLength(20)]
        public string FirstName { get; set; }

        [Required]
        [Column("Surname")]
        [MaxLength(20)]
        public string Surname { get; set; }

        [Required]
        [Column("LanguageId")]
        public int LanguageId { get; set; }

        [Required]
        [Column("TimeZone")]
        [MaxLength(50)]
        public string TimeZone { get; set; }

        [Required]
        [Column("Balance")]
        public decimal Balance { get; set; }
    }
}
