﻿// <copyright file="Seat.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Seat
    /// </summary>
    [Table("Seat")]
    public class Seat
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets id of area
        /// </summary>
        [Required]
        [Column("AreaId")]
        public int AreaId { get; set; }

        /// <summary>
        /// Gets or sets row
        /// </summary>
        [Required]
        [Column("Row")]
        public int Row { get; set; }

        /// <summary>
        /// Gets or sets number
        /// </summary>
        [Required]
        [Column("Number")]
        public int Number { get; set; }
    }
}