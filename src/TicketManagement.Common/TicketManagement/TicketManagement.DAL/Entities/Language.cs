﻿namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Language")]
    public class Language
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Required]
        [MaxLength(20)]
        [Column("Name")]
        public string Name{ get; set; }

        [Required]
        [MaxLength(2)]
        [Column("ShortName")]
        public string ShortName { get; set; }
    }
}
