﻿namespace TicketManagement.DAL.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("BookSeat")]
    public class BookSeat
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Required]
        [Column("EventSeatId")]
        public int EventSeatId { get; set; }

        [Required]
        [Column("UserId")]
        public string UserId { get; set; }

        [Required]
        [Column("Time")]
        public DateTimeOffset Time { get; set; }
    }
}
