﻿namespace TicketManagement.DAL.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// UserRole
    /// </summary>
    [Table("Purchases")]
    public class Purchase
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Foreign key Users
        /// </summary>
        [Required]
        [Column("UserId")]
        public string UserId { get; set; }

        /// <summary>
        /// Foreign key Users
        /// </summary>
        [Required]
        [Column("EventId")]
        public int EventId { get; set; }

        /// <summary>
        /// Cost of purchase
        /// </summary>
        [Required]
        [Column("Value")]
        public decimal Value { get; set; }

        /// <summary>
        /// Date of purchase
        /// </summary>
        [Required]
        [Column("Date")]
        public DateTimeOffset Date { get; set; }
    }
}
