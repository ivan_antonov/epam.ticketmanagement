﻿// <copyright file="EventSeat.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// EventSeat
    /// </summary>
    [Table("EventSeat")]
    public class EventSeat
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets event area of id
        /// </summary>
        [Required]
        [Column("EventAreaId")]
        public int EventAreaId { get; set; }

        /// <summary>
        /// Gets or sets row
        /// </summary>
        [Required]
        [Column("Row")]
        public int Row { get; set; }

        /// <summary>
        /// Gets or sets number
        /// </summary>
        [Required]
        [Column("Number")]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets state
        /// </summary>
        [Required]
        [Column("State")]
        public int State { get; set; }
    }
}