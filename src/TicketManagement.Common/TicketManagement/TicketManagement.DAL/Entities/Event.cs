﻿// <copyright file="Event.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Event
    /// </summary>
    [Table("Event")]
    public class Event
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        [Required]
        [MaxLength(120)]
        [Column("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        [Required]
        [Column("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets id of layout
        /// </summary>
        [Required]
        [Column("LayoutId")]
        public int LayoutId { get; set; }

        /// <summary>
        /// Gets or sets date start
        /// </summary>
        [Required]
        [Column("DateStart")]
        public DateTimeOffset DateStart { get; set; }

        /// <summary>
        /// Gets or sets date end
        /// </summary>
        [Required]
        [Column("DateEnd")]
        public DateTimeOffset DateEnd { get; set; }

        /// <summary>
        /// Gets or sets poster
        /// </summary>
        [Required]
        [Column("Poster")]
        [MaxLength(150)]
        public string Poster { get; set; }

        /// <summary>
        /// Gets or sets visible
        /// </summary>
        [Required]
        [Column("Visible")]
        public bool Visible { get; set; }
    }
}