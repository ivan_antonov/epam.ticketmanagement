// <copyright file="Layout.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Layout
    /// </summary>
    [Table("Layout")]
    public class Layout
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets id of venue
        /// </summary>
        [Required]
        [Column("VenueId")]
        public int VenueId { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        [Required]
        [MaxLength(90)]
        [Column("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        [Required]
        [MaxLength(120)]
        [Column("Description")]
        public string Description { get; set; }
    }
}