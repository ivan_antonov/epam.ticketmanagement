﻿namespace TicketManagement.DAL.Repositories
{
    using System.Data.Entity;
    using System.Linq;
    using EF;
    using Entities;
    using Interfaces;

    internal class UserRepository : IUserRepository
    {
        private TicketManagementContext context;
        private DbSet<User> dbSet;

        public UserRepository(TicketManagementContext context)
        {
            this.context = context;
            this.dbSet = context.Set<User>();
        }

        public void Create(User item)
        {
            dbSet.Add(item);
            context.SaveChanges();
        }

        public User Get(string id)
        {
            var item = dbSet.Find(id);
            if (item != null)
            {
                context.Entry(item).State = EntityState.Detached;
            }

            return item;
        }

        public IQueryable<User> GetAll()
        {
            return dbSet.AsNoTracking().AsQueryable();
        }

        public void Update(User item)
        {
            var old = dbSet.Find(item.Id);
            context.Entry(old).CurrentValues.SetValues(item);
            context.SaveChanges();
        }

        public void Delete(string id)
        {
            var item = dbSet.Find(id);
            if (item != null)
            {
                dbSet.Remove(item);
                context.SaveChanges();
            }
        }
    }
}