﻿// <copyright file="EventRepository.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Repositories
{
    using System.Data;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Linq;
    using EF;
    using Entities;
    using Interfaces;

    /// <summary>
    /// Event repository
    /// </summary>
    internal class EventRepository : IEventRepository
    {
        private readonly string connectionString;
        private TicketManagementContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventRepository"/> class.
        /// </summary>
        /// <param name="context">Context EF</param>
        public EventRepository(TicketManagementContext context)
        {
            this.db = context;
            this.connectionString = this.db.Database.Connection.ConnectionString;
        }

        /// <summary>
        /// Get list of Events
        /// </summary>
        /// <returns>List of events</returns>
        public IQueryable<Event> GetAll()
        {
            return this.db.Events.AsNoTracking().AsQueryable();
        }

        /// <summary>
        /// Get the Event by id
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>The event</returns>
        public Event Get(int id)
        {
            var item = db.Events.Find(id);
            if (item != null)
            {
                db.Entry(item).State = EntityState.Detached;
            }

            return item;
        }

        /// <summary>
        /// Create the Event in database
        /// </summary>
        /// <param name="itemEvent">the Event</param>
        public void Create(Event itemEvent)
        {
            var layoutIdParameter = new SqlParameter("@layoutid", itemEvent.LayoutId);
            var nameParameter = new SqlParameter("@name", itemEvent.Name);
            var descriptionParameter = new SqlParameter("@description", itemEvent.Description);
            var datestartParameter = new SqlParameter("@datestart", itemEvent.DateStart);
            var dateendParameter = new SqlParameter("@dateend", itemEvent.DateEnd);
            var posterParameter = new SqlParameter("@poster", itemEvent.Poster);

            this.db.Database.ExecuteSqlCommand(
                "CreateEvent @layoutid, @name, @description, @datestart, @dateend, @poster",
                layoutIdParameter, nameParameter, descriptionParameter, datestartParameter, dateendParameter, posterParameter);
            db.SaveChangesAsync();
        }

        /// <summary>
        /// Update the Event by id
        /// </summary>
        /// <param name="itemEvent">Updated Event</param>
        public void Update(Event itemEvent)
        {
            var idParameter = new SqlParameter("@id", itemEvent.Id);
            var nameParameter = new SqlParameter("@name", itemEvent.Name);
            var descriptionParameter = new SqlParameter("@description", itemEvent.Description);
            var datestartParameter = new SqlParameter("@datestart", itemEvent.DateStart);
            var dateendParameter = new SqlParameter("@dateend", itemEvent.DateEnd);
            var posterParameter = new SqlParameter("@poster", itemEvent.Poster);
            var visibleParameter = new SqlParameter("@visible", itemEvent.Visible);

            this.db.Database.ExecuteSqlCommand(
                "UpdateEvent @id, @name, @description, @datestart, @dateend, @poster, @visible",
                idParameter, nameParameter, descriptionParameter, datestartParameter, dateendParameter, posterParameter, visibleParameter);
            db.SaveChangesAsync();
        }

        /// <summary>
        /// Delete the Event by id
        /// </summary>
        /// <param name="id">Id</param>
        public void Delete(int id)
        {
            var idParameter = new SqlParameter("@id", id);
            this.db.Database.ExecuteSqlCommand("DeleteEvent @id", idParameter);
            db.SaveChangesAsync();
        }
    }
}