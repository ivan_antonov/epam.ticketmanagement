﻿namespace TicketManagement.DAL.Repositories
{
    using System.Data.Entity;
    using System.Linq;
    using EF;
    using Interfaces;

    internal class EFRepository<T> : IBaseRepository<T>
        where T : class
    {
        private TicketManagementContext context;
        private DbSet<T> dbSet;

        public EFRepository(TicketManagementContext context)
        {
            this.context = context;
            this.dbSet = context.Set<T>();
        }

        public void Create(T item)
        {
            dbSet.Add(item);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var item = dbSet.Find(id);
            if (item != null)
            {
                dbSet.Remove(item);
                context.SaveChanges();
            }
        }

        public T Get(int id)
        {
            var item = dbSet.Find(id);
            if (item != null)
            {
                context.Entry(item).State = EntityState.Detached;
            }

            return item;
        }

        public IQueryable<T> GetAll()
        {
            return dbSet.AsNoTracking().AsQueryable();
        }

        public void Update(T item)
        {
            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}