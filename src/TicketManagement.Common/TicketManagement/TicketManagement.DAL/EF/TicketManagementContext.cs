﻿// <copyright file="TicketManagementContext.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.EF
{
    using System.Data.Entity;
    using Entities;

    /// <summary>
    /// TicketManagement context
    /// </summary>
    public class TicketManagementContext : DbContext
    {
        /// <summary>
        /// Gets or sets Venues
        /// </summary>
        public DbSet<Venue> Venues { get; set; }

        /// <summary>
        /// Gets or sets Layouts
        /// </summary>
        public DbSet<Layout> Layouts { get; set; }

        /// <summary>
        /// Gets or sets Areas
        /// </summary>
        public DbSet<Area> Areas { get; set; }

        /// <summary>
        /// Gets or sets Seats
        /// </summary>
        public DbSet<Seat> Seats { get; set; }

        /// <summary>
        /// Gets or sets Events
        /// </summary>
        public DbSet<Event> Events { get; set; }

        /// <summary>
        /// Gets or sets EventAreas
        /// </summary>
        public DbSet<EventArea> EventAreas { get; set; }

        /// <summary>
        /// Gets or sets EventSeats
        /// </summary>
        public DbSet<EventSeat> EventSeats { get; set; }

        /// <summary>
        /// Gets or sets Users
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Gets or sets UserRoles
        /// </summary>
        public DbSet<UserRole> UserRoles { get; set; }

        /// <summary>
        /// Gets or sets Roles
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Gets or sets Purchases
        /// </summary>
        public DbSet<Purchase> Purchases { get; set; }

        /// <summary>
        /// Gets or sets Language
        /// </summary>
        public DbSet<Language> Languages { get; set; }

        /// <summary>
        /// Gets or sets BookSeat
        /// </summary>
        public DbSet<BookSeat> BookSeats { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TicketManagementContext"/> class.
        /// </summary>
        /// <param name="connection">Connection string</param>
        public TicketManagementContext(string connection)
            : base(connection)
        {
        }
    }
}