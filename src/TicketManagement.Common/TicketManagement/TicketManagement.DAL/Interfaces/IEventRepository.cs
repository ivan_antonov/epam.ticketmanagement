﻿// <copyright file="IEventRepository.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.DAL.Interfaces
{
    using System.Linq;
    using Entities;

    public interface IEventRepository
    {
        IQueryable<Event> GetAll();

        Event Get(int id);

        void Create(Event itemEvent);

        void Update(Event itemEvent);

        void Delete(int id);
    }
}