﻿namespace TicketManagement.DAL.Interfaces
{
    using System.Linq;
    using Entities;

    /// <summary>
    /// IBaseRepository
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Get list
        /// </summary>
        /// <returns></returns>
        IQueryable<User> GetAll();

        /// <summary>
        /// Get item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        User Get(string id);

        /// <summary>
        /// Create item
        /// </summary>
        /// <param name="user"></param>
        void Create(User user);

        /// <summary>
        /// Update item
        /// </summary>
        /// <param name="user"></param>
        void Update(User user);

        void Delete(string id);
    }
}