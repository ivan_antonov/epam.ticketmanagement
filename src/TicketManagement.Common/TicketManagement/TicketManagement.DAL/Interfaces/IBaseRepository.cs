﻿namespace TicketManagement.DAL.Interfaces
{
    using System.Linq;

    /// <summary>
    /// IBaseRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseRepository<T>
        where T : class
    {
        /// <summary>
        /// Get list
        /// </summary>
        /// <returns></returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Get item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Get(int id);

        /// <summary>
        /// Create item
        /// </summary>
        /// <param name="item"></param>
        void Create(T item);

        /// <summary>
        /// Update item
        /// </summary>
        /// <param name="item"></param>
        void Update(T item);

        /// <summary>
        /// Delete item
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);
    }
}