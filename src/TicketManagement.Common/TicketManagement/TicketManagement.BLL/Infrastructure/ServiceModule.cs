﻿// <copyright file="ServiceModule.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Infrastructure
{
    using AutoMapper;
    using DAL.EF;
    using DAL.Entities;
    using DAL.Interfaces;
    using DAL.Repositories;
    using Ninject;
    using Ninject.Modules;

    public class ServiceModule : NinjectModule
    {
        private string connectionString;

        public ServiceModule(string connection)
        {
            connectionString = connection;
        }

        public override void Load()
        {
            Bind<TicketManagementContext>().ToSelf().InThreadScope().WithConstructorArgument(connectionString);

            Bind<IBaseRepository<Venue>>().To<EFRepository<Venue>>();
            Bind<IBaseRepository<Layout>>().To<EFRepository<Layout>>();
            Bind<IBaseRepository<Area>>().To<EFRepository<Area>>();
            Bind<IBaseRepository<Seat>>().To<EFRepository<Seat>>();
            Bind<IBaseRepository<EventArea>>().To<EFRepository<EventArea>>();
            Bind<IBaseRepository<EventSeat>>().To<EFRepository<EventSeat>>();
            Bind<IEventRepository>().To<EventRepository>();
            Bind<IBaseRepository<Purchase>>().To<EFRepository<Purchase>>();

            Bind<IUserRepository>().To<UserRepository>();
            Bind<IBaseRepository<UserRole>>().To<EFRepository<UserRole>>();
            Bind<IBaseRepository<Role>>().To<EFRepository<Role>>();
            Bind<IBaseRepository<Language>>().To<EFRepository<Language>>();
            Bind<IBaseRepository<BookSeat>>().To<EFRepository<BookSeat>>();
        }
    }
}
