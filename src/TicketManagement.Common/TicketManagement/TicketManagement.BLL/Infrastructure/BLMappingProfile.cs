﻿// <copyright file="BLMappingProfile.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Infrastructure
{
    using AutoMapper;
    using DAL.Entities;
    using DTO;

    public class BLMappingProfile : Profile
    {
        public BLMappingProfile()
        {
            CreateMap<VenueDTO, Venue>().ReverseMap();
            CreateMap<LanguageDTO, Language>().ReverseMap();
            CreateMap<LayoutDTO, Layout>().ReverseMap();
            CreateMap<AreaDTO, Area>().ReverseMap();
            CreateMap<SeatDTO, Seat>().ReverseMap();
            CreateMap<EventDTO, Event>().ReverseMap();
            CreateMap<EventLayoutDTO, Layout>().ReverseMap();
            CreateMap<EventAreaDTO, EventArea>().ReverseMap();
            CreateMap<EventSeatDTO, EventSeat>().ReverseMap();
            CreateMap<BookSeat, BookSeatDTO>().ReverseMap();
        }
    }
}
