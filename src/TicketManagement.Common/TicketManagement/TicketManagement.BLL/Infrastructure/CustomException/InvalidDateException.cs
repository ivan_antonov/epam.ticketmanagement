﻿// <copyright file="InvalidDateException.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Infrastructure.CustomException
{
    using System;

    public class InvalidDateException : Exception
    {
        public string Property { get; protected set; }

        public InvalidDateException(string message, string prop)
            : base(message)
        {
            Property = prop;
        }
    }
}