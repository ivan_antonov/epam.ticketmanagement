﻿// <copyright file="InvalidDateException.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Infrastructure.CustomException
{
    using System;

    public class NotEnoughFundsException : Exception
    {
        public string Property { get; protected set; }

        public NotEnoughFundsException(string message, string prop)
            : base(message)
        {
            Property = prop;
        }
    }
}