﻿// <copyright file="InvalidPriceException.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Infrastructure
{
    using System;

    public class InvalidPriceException : Exception
    {
        public string Property { get; protected set; }

        public InvalidPriceException(string message, string prop)
            : base(message)
        {
            Property = prop;
        }
    }
}