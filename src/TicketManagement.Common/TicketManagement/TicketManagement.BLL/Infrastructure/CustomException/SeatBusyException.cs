﻿// <copyright file="InvalidDateException.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Infrastructure.CustomException
{
    using System;

    public class SeatBusyException : Exception
    {
        public SeatBusyException(string message)
            : base(message)
        {
        }
    }
}