﻿// <copyright file="UniqueArgumentException.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Infrastructure
{
    using System;

    public class UniqueArgumentException : Exception
    {
        public string Property { get; protected set; }

        public UniqueArgumentException(string message, string prop)
            : base(message)
        {
            Property = prop;
        }
    }
}
