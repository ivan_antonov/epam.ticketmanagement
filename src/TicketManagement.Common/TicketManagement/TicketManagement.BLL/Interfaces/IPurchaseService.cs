﻿// <copyright file="ISeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Interfaces
{
    using System.Linq;
    using DTO;

    public interface IPurchaseService
    {
        IQueryable<EventPurchaseDTO> GetEventPurchasesById(string id);

        void BuySeats(int eventId, string userId);
    }
}