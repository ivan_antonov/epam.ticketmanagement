﻿// <copyright file="IVenueService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Interfaces
{
    using System.Linq;
    using DTO;

    public interface IVenueService
    {
        IQueryable<VenueDTO> GetAll();

        VenueDTO Get(int id);

        void Create(VenueDTO venue);

        void Update(VenueDTO venue);

        void Delete(int id);
    }
}