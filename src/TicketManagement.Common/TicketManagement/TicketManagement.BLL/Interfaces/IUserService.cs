﻿namespace TicketManagement.BLL.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;
    using DTO;

    public interface IUserService
    {
        UserDTO GetUserById(string id);

        UserDTO GetUserByUserName(string name);

        IQueryable<string> GetRoles(string name);

        IQueryable<LanguageDTO> GetLanguages();

        void Create(UserDTO userDto);

        void Update(UserDTO userDto);

        void Delete(string id);

    }
}