﻿// <copyright file="IAreaService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Interfaces
{
    using System.Linq;
    using DTO;

    public interface IAreaService
    {
        IQueryable<AreaDTO> GetAll();

        AreaDTO Get(int id);

        void Create(AreaDTO area);

        void Update(AreaDTO area);

        void Delete(int id);
    }
}