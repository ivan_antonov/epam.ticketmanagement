﻿// <copyright file="IEventService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Interfaces
{
    using System.Linq;
    using DTO;

    public interface IEventService
    {
        IQueryable<EventDTO> GetAll();

        EventDTO Get(int id);

        EventDTO GetByUser(int id, string user);

        IQueryable<EventAreaDTO> GetAllArea();

        void Create(EventDTO eventDto);

        void Update(EventDTO eventDto);

        void UpdateArea(EventAreaDTO eventDto);

        void Delete(int id);
    }
}