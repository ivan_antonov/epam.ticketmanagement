﻿// <copyright file="ISeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Interfaces
{
    using System.Linq;
    using DTO;

    public interface ISeatService
    {
        IQueryable<SeatDTO> GetAll();

        SeatDTO Get(int id);

        void Create(SeatDTO seat);

        void Update(SeatDTO seat);

        void Delete(int id);
    }
}