﻿// <copyright file="ILayoutService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Interfaces
{
    using System.Linq;
    using DTO;

    public interface ILayoutService
    {
        IQueryable<LayoutDTO> GetAll();

        IQueryable<LayoutDTO> GetAllByVenueId(int id);

        LayoutDTO Get(int id);

        void Create(LayoutDTO layout);

        void Update(LayoutDTO layout);

        void Delete(int id);
    }
}