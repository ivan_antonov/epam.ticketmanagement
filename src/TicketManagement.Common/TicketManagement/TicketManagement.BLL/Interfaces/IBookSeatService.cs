﻿// <copyright file="ISeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Interfaces
{
    using System.Linq;
    using DTO;

    public interface IBookSeatService
    {
        IQueryable<BookSeatDTO> GetAll();

        BookSeatDTO Get(int id);

        void Create(BookSeatDTO seat);

        void Delete(int id);
    }
}