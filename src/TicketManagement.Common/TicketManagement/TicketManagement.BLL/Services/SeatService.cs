﻿// <copyright file="SeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Infrastructure;
    using Interfaces;

    internal class SeatService : ISeatService
    {
        private readonly IBaseRepository<Seat> seatRepository;
        private IMapper map;

        public SeatService(IMapper mapper, IBaseRepository<Seat> seatRepository)
        {
            map = mapper;
            this.seatRepository = seatRepository;
        }

        public void Create(SeatDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            IsUnique(item);
            var seat = map.Map<SeatDTO, Seat>(item);
            seatRepository.Create(seat);
        }

        private void IsUnique(SeatDTO item)
        {
            var isUnique = from t in seatRepository.GetAll()
                           where t.AreaId.Equals(item.AreaId)
                                 && t.Row == item.Row
                                 && t.Number == item.Number
                           select t;

            if (isUnique.Any())
            {
                throw new UniqueArgumentException("The number must be unique", nameof(item));
            }
        }

        public void Delete(int id)
        {
            seatRepository.Delete(id);
        }

        public SeatDTO Get(int id)
        {
            var seat = seatRepository.Get(id);
            if (seat == null)
            {
                throw new NullReferenceException();
            }

            return map.Map<Seat, SeatDTO>(seat);
        }

        public IQueryable<SeatDTO> GetAll()
        {
            return map.Map<IQueryable<Seat>, List<SeatDTO>>(seatRepository.GetAll()).AsQueryable();
        }

        public void Update(SeatDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            IsUnique(item);

            seatRepository.Update(map.Map<SeatDTO, Seat>(item));
        }
    }
}
