﻿namespace TicketManagement.BLL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Interfaces;

    internal class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IBaseRepository<UserRole> userRoleRepository;
        private readonly IBaseRepository<Role> roleRepository;
        private readonly IBaseRepository<Language> languageRepository;
        private readonly IMapper map;

        public UserService(IUserRepository userRepository, IMapper map, IBaseRepository<UserRole> userRoleRepository, IBaseRepository<Role> roleRepository, IBaseRepository<Language> languageRepository)
        {
            this.userRepository = userRepository;
            this.map = map;
            this.userRoleRepository = userRoleRepository;
            this.roleRepository = roleRepository;
            this.languageRepository = languageRepository;
        }

        public UserDTO GetUserById(string id)
        {

            return map.Map<User, UserDTO>(userRepository.Get(id));
        }

        public UserDTO GetUserByUserName(string name)
        {
            var user = userRepository.GetAll().FirstOrDefault(model => model.UserName == name);
            return map.Map<User, UserDTO>(user);
        }

        public IQueryable<string> GetRoles(string name)
        {
            var roles = from user in userRepository.GetAll()
                        join userRole in userRoleRepository.GetAll() on user.Id equals userRole.UserId
                        join role in roleRepository.GetAll() on userRole.RoleId equals role.Id
                        where user.UserName == name
                        select role.Name.Trim();
            return roles;
        }

        public IQueryable<LanguageDTO> GetLanguages()
        {
            var languages = languageRepository.GetAll();
            return map.Map<IQueryable<Language>, List<LanguageDTO>>(languages).AsQueryable();
        }

        public void Create(UserDTO userDto)
        {
            var user = map.Map<UserDTO, User>(userDto);
            var roleId = from role in roleRepository.GetAll()
                         where role.Name == "User"
                         select role.Id;
            userRepository.Create(user);
            var userRole = new UserRole
            {
                RoleId = roleId.FirstOrDefault(),
                UserId = user.Id
            };
            userRoleRepository.Create(userRole);
        }

        public void Update(UserDTO userDto)
        {
            var user = map.Map<UserDTO, User>(userDto);
            userRepository.Update(user);
        }

        public void Delete(string id)
        {
            userRepository.Delete(id);
        }
    }
}
