﻿// <copyright file="LayoutService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Infrastructure;
    using Interfaces;

    internal class LayoutService : ILayoutService
    {
        private readonly IBaseRepository<Layout> layotRepository;
        private IMapper map;

        public LayoutService(IMapper mapper, IBaseRepository<Layout> db)
        {
            map = mapper;
            this.layotRepository = db;
        }

        public void Create(LayoutDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            IsUnique(item);

            var layout = map.Map<LayoutDTO, Layout>(item);
            layotRepository.Create(layout);
        }

        private void IsUnique(LayoutDTO item)
        {

            var isUnique = from t in layotRepository.GetAll()
                           where t.VenueId.Equals(item.VenueId) &&
                                 string.Compare(t.Name, item.Name, StringComparison.OrdinalIgnoreCase) == 0
                           select t;

            if (isUnique.Any())
            {
                throw new UniqueArgumentException("The name must be unique", nameof(item));
            }
        }

        public void Delete(int id)
        {
            layotRepository.Delete(id);
        }

        public IQueryable<LayoutDTO> GetAllByVenueId(int id)
        {
            var layouts = from l in layotRepository.GetAll()
                          where l.VenueId == id
                          select l;
            return map.Map<IQueryable<Layout>, List<LayoutDTO>>(layouts).AsQueryable();
        }

        public LayoutDTO Get(int id)
        {
            var layout = layotRepository.Get(id);
            if (layout == null)
            {
                throw new NullReferenceException();
            }

            return map.Map<Layout, LayoutDTO>(layout);
        }

        public IQueryable<LayoutDTO> GetAll()
        {
            return map.Map<IQueryable<Layout>, List<LayoutDTO>>(layotRepository.GetAll()).AsQueryable();
        }

        public void Update(LayoutDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            IsUnique(item);

            layotRepository.Update(map.Map<LayoutDTO, Layout>(item));
        }
    }
}
