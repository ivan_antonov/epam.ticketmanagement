﻿// <copyright file="SeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Services
{
    using System;
    using System.Linq;
    using AutoMapper;
    using DAL.EF;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Enums;
    using Infrastructure.CustomException;
    using Interfaces;

    internal class PurchaseService : IPurchaseService
    {
        private readonly IBaseRepository<Purchase> purchaseRepository;
        private readonly IEventRepository eventRepository;
        private readonly IBaseRepository<EventSeat> eventSeatRepository;
        private readonly IBaseRepository<EventArea> eventAreaRepository;
        private readonly IBaseRepository<BookSeat> bookRepository;
        private readonly IUserRepository userRepository;
        private readonly TicketManagementContext context;

        private IMapper map;

        public PurchaseService(IMapper mapper, IBaseRepository<Purchase> purchaseRepository, IEventRepository eventRepository, IBaseRepository<BookSeat> bookRepository, IBaseRepository<EventSeat> eventSeatRepository, IBaseRepository<EventArea> eventAreaRepository, TicketManagementContext context, IUserRepository userRepository)
        {
            map = mapper;
            this.purchaseRepository = purchaseRepository;
            this.eventRepository = eventRepository;
            this.bookRepository = bookRepository;
            this.eventSeatRepository = eventSeatRepository;
            this.eventAreaRepository = eventAreaRepository;
            this.context = context;
            this.userRepository = userRepository;
        }

        public IQueryable<EventPurchaseDTO> GetEventPurchasesById(string id)
        {
            var events = from e in eventRepository.GetAll()
                         join purchase in purchaseRepository.GetAll() on e.Id equals purchase.EventId
                         where purchase.UserId == id
                         select new EventPurchaseDTO()
                         {
                             Id = e.Id,
                             Name = e.Name,
                             Description = e.Description,
                             Poster = e.Poster,
                             Price = purchase.Value,
                             DatePurchase = purchase.Date
                         };
            return events.AsQueryable();
        }

        public void BuySeats(int eventId, string userId)
        {
            var seats = from seat in bookRepository.GetAll()
                        join eventSeat in eventSeatRepository.GetAll() on seat.EventSeatId equals eventSeat.Id
                        join eventArea in eventAreaRepository.GetAll() on eventSeat.EventAreaId equals eventArea.Id
                        where seat.UserId == userId && eventArea.EventId == eventId
                        select new BookSeatDTO
                        {
                            EventSeatId = seat.EventSeatId,
                            Id = seat.Id,
                            Price = eventArea.Price
                        };

            var summaryPrice = seats.Sum(bookSeatDto => bookSeatDto.Price);
            var user = userRepository.Get(userId);
            if (user.Balance - summaryPrice < 0)
            {
                throw new NotEnoughFundsException("Not enough funds in the account", nameof(user));
            }

            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    foreach (var seat in seats.ToList())
                    {
                        var purchase = new Purchase
                        {
                            Date = DateTimeOffset.Now,
                            EventId = eventId,
                            UserId = userId,
                            Value = seat.Price
                        };

                        purchaseRepository.Create(purchase);
                        var eventSeat = eventSeatRepository.Get(seat.EventSeatId);
                        eventSeat.State = (int)State.Busy;
                        eventSeatRepository.Update(eventSeat);
                        bookRepository.Delete(seat.Id);
                    }

                    user.Balance -= summaryPrice;
                    userRepository.Update(user);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }
    }
}
