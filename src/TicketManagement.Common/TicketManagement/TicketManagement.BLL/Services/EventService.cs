﻿// <copyright file="EventService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Enums;
    using Infrastructure.CustomException;
    using Interfaces;

    internal class EventService : IEventService
    {
        private readonly IEventRepository eventRepository;
        private readonly IBaseRepository<BookSeat> bookRepository;
        private readonly IBaseRepository<EventSeat> eventSeatRepository;
        private readonly IBaseRepository<EventArea> eventAreaRepository;
        private readonly IBaseRepository<Layout> layoutRepository;
        private readonly IBaseRepository<Venue> venueRepository;
        private readonly IBaseRepository<Area> areaRepository;
        private readonly IBaseRepository<Seat> seatRepository;

        private IMapper map;

        public EventService(
            IMapper mapper,
            IEventRepository eventRepository,
            IBaseRepository<EventSeat> eventSeatRepository,
            IBaseRepository<EventArea> eventAreaRepository,
            IBaseRepository<Layout> layoutRepository,
            IBaseRepository<Venue> venueRepository,
            IBaseRepository<Area> areaRepository,
            IBaseRepository<Seat> seatRepository,
            IBaseRepository<BookSeat> bookRepository)
        {
            map = mapper;
            this.eventRepository = eventRepository;
            this.eventAreaRepository = eventAreaRepository;
            this.eventSeatRepository = eventSeatRepository;
            this.layoutRepository = layoutRepository;
            this.venueRepository = venueRepository;
            this.areaRepository = areaRepository;
            this.bookRepository = bookRepository;
            this.seatRepository = seatRepository;
        }

        public EventDTO GetByUser(int id, string user)
        {
            var events = Get(id);
            foreach (var eventAreaDto in events.Layout.Areas)
            {
                foreach (var eventSeatDto in eventAreaDto.Seats)
                {
                    var seats = bookRepository.GetAll().FirstOrDefault(seat => seat.EventSeatId == eventSeatDto.Id);
                    if (seats == null)
                    {
                        continue;
                    }

                    eventSeatDto.State = seats.UserId == user ? (int)State.Lock : (int)State.Busy;
                }
            }

            return events;

        }

        public IQueryable<EventAreaDTO> GetAllArea()
        {
            var areas = eventAreaRepository.GetAll();
            return map.Map<IQueryable<EventArea>, IList<EventAreaDTO>>(areas).AsQueryable();
        }

        public void Create(EventDTO item)
        {
            IsVacant(item);
            var Event = map.Map<EventDTO, Event>(item);
            eventRepository.Create(Event);
        }

        public void UpdateArea(EventAreaDTO eventDto)
        {
            var eventArea = eventAreaRepository.Get(eventDto.Id);
            eventArea.Price = eventDto.Price;
            eventAreaRepository.Update(eventArea);
            var isNotSetPrice = eventAreaRepository.GetAll()
                .Any(area => area.EventId == eventArea.EventId && area.Price == 0);
            if (!isNotSetPrice)
            {
                var eventForArea = eventRepository.Get(eventArea.EventId);
                eventForArea.Visible = true;
                eventRepository.Update(eventForArea);
            }
        }

        public void Delete(int id)
        {
            var isBusy = from e in eventRepository.GetAll()
                         where e.Id == id
                         join a in eventAreaRepository.GetAll() on e.Id equals a.EventId
                         join s in eventSeatRepository.GetAll() on a.Id equals s.EventAreaId
                         where s.State == (int)State.Busy
                         select s;

            if (isBusy.Any())
            {
                throw new SeatBusyException("Seat is busy");
            }

            eventRepository.Delete(id);
        }

        public EventDTO Get(int id)
        {
            var eventDal = eventRepository.Get(id);
            if (eventDal == null)
            {
                throw new NullReferenceException();
            }

            var eventDto = map.Map<Event, EventDTO>(eventDal);
            eventDto.Layout = map.Map<Layout, EventLayoutDTO>(layoutRepository.Get(eventDto.LayoutId));
            eventDto.Layout.Venue = map.Map<Venue, VenueDTO>(venueRepository.Get(eventDto.Layout.VenueId));
            eventDto.Layout.Areas = map.Map<IQueryable<EventArea>, List<EventAreaDTO>>(eventAreaRepository.GetAll().Where(area => area.EventId == eventDto.Id));

            foreach (var eventAreaDto in eventDto.Layout.Areas)
            {
                eventAreaDto.Seats = map.Map<IQueryable<EventSeat>, List<EventSeatDTO>>(eventSeatRepository.GetAll()
                    .Where(seat => seat.EventAreaId == eventAreaDto.Id));
            }

            return eventDto;
        }

        public IQueryable<EventDTO> GetAll()
        {
            var events = from e in eventRepository.GetAll()
                         join l in layoutRepository.GetAll() on e.LayoutId equals l.Id
                         join v in venueRepository.GetAll() on l.VenueId equals v.Id
                         select new EventDTO()
                         {
                             Id = e.Id,
                             Name = e.Name,
                             Description = e.Description,
                             DateStart = e.DateStart,
                             DateEnd = e.DateEnd,
                             Poster = e.Poster,
                             Visible = e.Visible,
                             Layout = new EventLayoutDTO()
                             {
                                 Id = l.Id,
                                 Name = l.Name,
                                 Description = l.Description,
                                 Venue = new VenueDTO()
                                 {
                                     Id = v.Id,
                                     Name = v.Name,
                                     Description = v.Description,
                                     Address = v.Address,
                                     Phone = v.Phone
                                 }
                             }
                         };

            return events.AsQueryable();
        }

        public void Update(EventDTO item)
        {
            IsVacant(item);
            var eventCheck = eventRepository.Get(item.Id);
            if (eventCheck.LayoutId == item.LayoutId)
            {
                eventRepository.Update(map.Map<EventDTO, Event>(item));
            }
            else
            {
                var areas = from area in eventAreaRepository.GetAll()
                            where area.EventId == item.Id
                            select area;

                var isStateSeat = from area in areas
                                  join seat in eventSeatRepository.GetAll() on area.Id equals seat.EventAreaId
                                  where seat.State == (int)State.Busy
                                  select seat;
                if (!isStateSeat.Any())
                {
                    eventRepository.Delete(item.Id);
                    eventRepository.Create(map.Map<EventDTO, Event>(item));
                }

            }
        }

        private void IsVacant(EventDTO eventDto)
        {
            if (eventDto == null)
            {
                throw new NullReferenceException();
            }

            if (eventDto.DateStart < DateTimeOffset.Now)
            {
                throw new InvalidDateException("Can not create event in the past", nameof(eventDto));
            }

            if (eventDto.DateStart > eventDto.DateEnd)
            {
                throw new InvalidDateException("Dates are incorrect", nameof(eventDto));
            }

            if (eventRepository.GetAll().Any(item => item.LayoutId == eventDto.LayoutId
                                                     && ((item.DateStart < eventDto.DateEnd && item.DateStart > eventDto.DateStart)
                                                         || (item.DateEnd < eventDto.DateEnd && item.DateEnd > eventDto.DateStart))))
            {
                throw new InvalidDateException("The time is not vacant", nameof(eventDto));
            }

            var isSeats = from t in areaRepository.GetAll()
                          join s in seatRepository.GetAll() on t.Id equals s.AreaId
                          where t.LayoutId == eventDto.LayoutId
                          select s;

            if (!isSeats.Any())
            {
                throw new Exception("Event can't be created without any seat");
            }
        }
    }
}
