﻿// <copyright file="SeatService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Services
{
    using System;
    using System.Linq;
    using AutoMapper;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Interfaces;

    internal class BookSeatService : IBookSeatService
    {
        private readonly IBaseRepository<BookSeat> seatRepository;
        private readonly IBaseRepository<EventArea> eventAreaRepository;
        private readonly IBaseRepository<EventSeat> eventSeatRepository;

        private IMapper map;

        public BookSeatService(IMapper mapper, IBaseRepository<BookSeat> seatRepository, IBaseRepository<EventArea> eventAreaRepository, IBaseRepository<EventSeat> eventSeatRepository)
        {
            map = mapper;
            this.seatRepository = seatRepository;
            this.eventAreaRepository = eventAreaRepository;
            this.eventSeatRepository = eventSeatRepository;
        }

        public void Create(BookSeatDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            var seat = map.Map<BookSeatDTO, BookSeat>(item);
            seatRepository.Create(seat);
        }

        public void Delete(int id)
        {
            seatRepository.Delete(id);
        }

        public BookSeatDTO Get(int id)
        {
            var seat = seatRepository.Get(id);
            if (seat == null)
            {
                throw new NullReferenceException();
            }

            return map.Map<BookSeat, BookSeatDTO>(seat);
        }

        public IQueryable<BookSeatDTO> GetAll()
        {
            var bookSeats = from seat in seatRepository.GetAll()
                            join eventSeat in eventSeatRepository.GetAll() on seat.EventSeatId equals eventSeat.Id
                            join area in eventAreaRepository.GetAll() on eventSeat.EventAreaId equals area.Id
                            select new BookSeatDTO
                            {
                                Id = seat.Id,
                                EventSeatId = eventSeat.Id,
                                UserId = seat.UserId,
                                Time = seat.Time,
                                Price = area.Price,
                                Row = eventSeat.Row,
                                Number = eventSeat.Number
                            };
            return bookSeats;
        }

    }
}
