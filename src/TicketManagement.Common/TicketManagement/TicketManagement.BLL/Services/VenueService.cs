﻿// <copyright file="VenueService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Infrastructure;
    using Interfaces;

    internal class VenueService : IVenueService
    {
        private readonly IBaseRepository<Venue> venueRepository;
        private IMapper map;

        public VenueService(IMapper mapper, IBaseRepository<Venue> db)
        {
            map = mapper;
            this.venueRepository = db;
        }

        public IQueryable<VenueDTO> GetAll()
        {
            return map.Map<IQueryable<Venue>, List<VenueDTO>>(venueRepository.GetAll()).AsQueryable();
        }

        public VenueDTO Get(int id)
        {
            var venue = venueRepository.Get(id);
            if (venue == null)
            {
                throw new NullReferenceException();
            }

            return map.Map<Venue, VenueDTO>(venue);
        }

        public void Create(VenueDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            IsUnique(item);
            var venue = map.Map<VenueDTO, Venue>(item);
            venueRepository.Create(venue);
        }

        public void Update(VenueDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            IsUnique(item);
            venueRepository.Update(map.Map<VenueDTO, Venue>(item));
        }

        public void Delete(int id)
        {
            venueRepository.Delete(id);
        }

        private void IsUnique(VenueDTO checkVenue)
        {

            var isUnique = from t in venueRepository.GetAll()
                           where string.Compare(t.Name.Trim(), checkVenue.Name.Trim(), StringComparison.OrdinalIgnoreCase) == 0
                           select t;

            if (isUnique.Any())
            {
                throw new UniqueArgumentException("The name must be unique", nameof(checkVenue));
            }
        }
    }
}
