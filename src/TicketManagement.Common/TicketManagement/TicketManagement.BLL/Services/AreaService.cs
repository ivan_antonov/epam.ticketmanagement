﻿// <copyright file="AreaService.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using DAL.Entities;
    using DAL.Interfaces;
    using DTO;
    using Infrastructure;
    using Interfaces;

    internal class AreaService : IAreaService
    {
        private readonly IBaseRepository<Area> areaRepository;
        private IMapper map;

        public AreaService(IMapper mapper, IBaseRepository<Area> areaRepository)
        {
            map = mapper;
            this.areaRepository = areaRepository;
        }

        public void Create(AreaDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            IsUnique(item);

            var area = map.Map<AreaDTO, Area>(item);
            areaRepository.Create(area);
        }

        public void Delete(int id)
        {
            areaRepository.Delete(id);
        }

        public AreaDTO Get(int id)
        {
            var area = areaRepository.Get(id);
            if (area == null)
            {
                throw new NullReferenceException();
            }

            return map.Map<Area, AreaDTO>(area);
        }

        public IQueryable<AreaDTO> GetAll()
        {
            return map.Map<IQueryable<Area>, List<AreaDTO>>(areaRepository.GetAll()).AsQueryable();
        }

        public void Update(AreaDTO item)
        {
            if (item == null)
            {
                throw new NullReferenceException();
            }

            IsUnique(item);

            areaRepository.Update(map.Map<AreaDTO, Area>(item));
        }

        private void IsUnique(AreaDTO item)
        {

            var isUnique = from t in areaRepository.GetAll()
                           where t.LayoutId.Equals(item.LayoutId) &&
                                 string.Compare(t.Description, item.Description, StringComparison.OrdinalIgnoreCase) == 0
                           select t;

            if (isUnique.Any())
            {
                throw new UniqueArgumentException("The description must be unique", nameof(item));
            }
        }
    }
}
