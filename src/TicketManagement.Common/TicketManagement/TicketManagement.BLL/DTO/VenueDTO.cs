﻿// <copyright file="VenueDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.DTO
{
    using System.Collections.Generic;

    /// <summary>
    /// Venue
    /// </summary>
    public class VenueDTO
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets  address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets list of layouts
        /// </summary>
        public List<LayoutDTO> Layouts { get; set; }
    }
}
