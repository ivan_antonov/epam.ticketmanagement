﻿// <copyright file="SeatDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.DTO
{
    /// <summary>
    /// Seat
    /// </summary>
    public class SeatDTO
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets id of area
        /// </summary>
        public int AreaId { get; set; }

        /// <summary>
        /// Gets or sets row
        /// </summary>
        public int Row { get; set; }

        /// <summary>
        /// Gets or sets number
        /// </summary>
        public int Number { get; set; }
    }
}