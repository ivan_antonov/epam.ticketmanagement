﻿namespace TicketManagement.BLL.DTO
{
    public class UserDTO
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public int LanguageId { get; set; }

        public string TimeZone { get; set; }

        public decimal Balance { get; set; }
    }
}
