﻿namespace TicketManagement.BLL.DTO
{
    using System;

    public class BookSeatDTO
    {
        public int Id { get; set; }

        public int EventSeatId { get; set; }

        public string UserId { get; set; }

        public DateTimeOffset Time { get; set; }

        public decimal Price { get; set; }

        public int Row { get; set; }

        public int Number { get; set; }
    }
}
