﻿namespace TicketManagement.BLL.DTO
{
    public class LanguageDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }
    }
}
