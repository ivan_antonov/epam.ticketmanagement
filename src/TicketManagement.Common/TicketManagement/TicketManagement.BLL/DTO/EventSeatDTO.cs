﻿// <copyright file="EventSeatDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.DTO
{
    /// <summary>
    /// EventSeat
    /// </summary>
    public class EventSeatDTO
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets event area of id
        /// </summary>
        public int EventAreaId { get; set; }

        /// <summary>
        /// Gets or sets row
        /// </summary>
        public int Row { get; set; }

        /// <summary>
        /// Gets or sets number
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets state
        /// </summary>
        public int State { get; set; }
    }
}