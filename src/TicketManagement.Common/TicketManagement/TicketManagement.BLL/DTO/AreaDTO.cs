﻿// <copyright file="AreaDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.DTO
{
    using System.Collections.Generic;

    /// <summary>
    /// Area
    /// </summary>
    public class AreaDTO
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Id of layout
        /// </summary>
        public int LayoutId { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets CoordX
        /// </summary>
        public int CoordX { get; set; }

        /// <summary>
        /// Gets or sets CoordY
        /// </summary>
        public int CoordY { get; set; }

        /// <summary>
        /// Gets or sets seats
        /// </summary>
        public List<SeatDTO> Seats { get; set; }
    }
}