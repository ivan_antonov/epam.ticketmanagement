﻿// <copyright file="EventAreaDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.DTO
{
    using System.Collections.Generic;

    /// <summary>
    /// EventArea
    /// </summary>
    public class EventAreaDTO
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets id of event
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets coordinate x
        /// </summary>
        public int CoordX { get; set; }

        /// <summary>
        /// Gets or sets coordinate y
        /// </summary>
        public int CoordY { get; set; }

        /// <summary>
        /// Gets or sets price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets list's seats
        /// </summary>
        public List<EventSeatDTO> Seats { get; set; }
    }
}