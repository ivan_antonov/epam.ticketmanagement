﻿// <copyright file="EventDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.DTO
{
    using System;

    /// <summary>
    /// Event
    /// </summary>
    public class EventDTO
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets id of layout
        /// </summary>
        public int LayoutId { get; set; }

        /// <summary>
        /// Gets or sets date start
        /// </summary>
        public DateTimeOffset DateStart { get; set; }

        /// <summary>
        /// Gets or sets date end
        /// </summary>
        public DateTimeOffset DateEnd { get; set; }

        /// <summary>
        /// Gets or sets poster
        /// </summary>
        public string Poster { get; set; }

        /// <summary>
        /// Gets or sets visible
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets layout
        /// </summary>
        public EventLayoutDTO Layout { get; set; }
    }
}