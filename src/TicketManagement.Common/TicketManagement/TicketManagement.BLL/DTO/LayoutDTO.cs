﻿// <copyright file="LayoutDTO.cs" company="Ivan Antonov">
// Copyright (c) Ivan Antonov. All rights reserved.
// </copyright>

namespace TicketManagement.BLL.DTO
{
    using System.Collections.Generic;

    /// <summary>
    /// Layout
    /// </summary>
    public class LayoutDTO
    {
        /// <summary>
        /// Gets or sets id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets id of venue
        /// </summary>
        public int VenueId { get; set; }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets areas
        /// </summary>
        public List<AreaDTO> Areas { get; set; }
    }
}
