﻿namespace TicketManagement.BLL.Enums
{
    public enum State
    {
        Free = 0,
        Lock,
        Busy
    }
}