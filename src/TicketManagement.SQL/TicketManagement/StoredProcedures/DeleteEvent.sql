﻿--This stored procedure deletes the event  by id in the event table
CREATE PROCEDURE [dbo].[DeleteEvent]
	@id int
AS
BEGIN
BEGIN TRY
	    SET NOCOUNT, XACT_ABORT ON
		BEGIN TRAN
	DELETE FROM [dbo].[Event]
    WHERE [dbo].[Event].Id = @id
		COMMIT TRAN
	END TRY
	BEGIN CATCH

		if @@TRANCOUNT > 0
			ROLLBACK TRAN
		   
	END CATCH
END
RETURN 0