﻿--This stored procedure updates the event by id in the event table
CREATE PROCEDURE [dbo].[UpdateEvent]
	@id int,
    @name nvarchar(120),
    @description nvarchar(MAX),
	@datestart DATETIMEOFFSET,
	@dateend DATETIMEOFFSET,
	@poster nvarchar(150),
	@visible bit
AS
	UPDATE [dbo].[Event]
    SET [Name] = @name,
		[Description] = @description,
		[DateStart] = @datestart,
		[DateEnd] = @dateend,
		[Poster] = @poster,
		[Visible] = @visible
    WHERE [dbo].[Event].Id = @id
RETURN 0