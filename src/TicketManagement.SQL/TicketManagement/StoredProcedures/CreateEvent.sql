﻿--This stored procedure adds a event to the event table
CREATE PROCEDURE [dbo].[CreateEvent]
	@layoutid int,
	@name nvarchar(120),
	@description nvarchar(max),
	@datestart DATETIMEOFFSET,
	@dateend DATETIMEOFFSET,
	@poster nvarchar(150)
AS
BEGIN
	BEGIN TRY
	    SET NOCOUNT, XACT_ABORT ON
		BEGIN TRAN
   
			DECLARE @eventId int,
			        @state int,
			        @price decimal,
					@visible bit

			SET @visible = 0
			INSERT INTO [dbo].[Event]([LayoutId], [Name], [Description],[DateStart], [DateEnd], [Poster], [Visible])
			VALUES(@layoutid, @name, @description, @datestart, @dateend, @poster, @visible)
	
			SET @eventId = SCOPE_IDENTITY()
			SET @price = 0
			

			INSERT INTO [dbo].[EventArea] ([Description], [CoordX], [CoordY], [EventId], [Price]) 
			SELECT [dbo].[Area].[Description], [dbo].[Area].[CoordX], [dbo].[Area].[CoordY], @eventId, @price
			FROM [dbo].[Area] 
			WHERE ([dbo].[Area].[LayoutId] = @layoutid)

			SET @state = 0
		    INSERT INTO [dbo].[EventSeat] ([Number], [Row], [EventAreaId], [State]) 
            SELECT [dbo].[Seat].[Number], [dbo].[Seat].[Row], [dbo].[EventArea].[Id], @state
            FROM [dbo].[Area] INNER JOIN [dbo].[Seat] ON [dbo].[Area].[Id] = [dbo].[Seat].[AreaId] 
							  INNER JOIN [dbo].[EventArea] ON [dbo].[EventArea].[Description] = [dbo].[Area].[Description]
			WHERE ([dbo].[EventArea].[EventId] = @eventId) AND ([dbo].[Area].[LayoutId] = @layoutid)
			
		COMMIT TRAN
	END TRY
	BEGIN CATCH

		if @@TRANCOUNT > 0
			ROLLBACK TRAN
		   
	END CATCH
END

RETURN 0
