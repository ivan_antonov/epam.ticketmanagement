﻿ALTER TABLE dbo.BookSeat
ADD CONSTRAINT FK_EventSeat_BookSeat FOREIGN KEY ([EventSeatId])     
    REFERENCES dbo.EventSeat (Id)
	ON DELETE CASCADE