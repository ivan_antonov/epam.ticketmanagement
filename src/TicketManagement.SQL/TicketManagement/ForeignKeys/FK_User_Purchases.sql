﻿ALTER TABLE dbo.Purchases
ADD CONSTRAINT FK_User_Purchases FOREIGN KEY (UserId)     
    REFERENCES dbo.Users (Id)
	ON DELETE CASCADE