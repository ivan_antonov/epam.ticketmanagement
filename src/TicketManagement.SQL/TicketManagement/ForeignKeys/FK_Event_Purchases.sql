﻿ALTER TABLE dbo.Purchases
ADD CONSTRAINT FK_Event_Purchases FOREIGN KEY (EventId)     
    REFERENCES dbo.[Event] (Id)
	ON DELETE CASCADE