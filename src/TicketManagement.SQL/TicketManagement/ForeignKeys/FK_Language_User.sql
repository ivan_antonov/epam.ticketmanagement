﻿ALTER TABLE dbo.Users
ADD CONSTRAINT FK_Language_User FOREIGN KEY (LanguageId)     
    REFERENCES dbo.[Language] (Id)
	ON DELETE CASCADE