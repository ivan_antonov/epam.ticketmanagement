﻿--- Venue
insert into dbo.Venue
values ('Landmark Theatres','5 Screens DLP Digital Projection and Sound. Opened December 21, 2001. Built in 1898.', '143 East Houston Street between 1st and 2nd Avenues', '(212) 260-7289'),
	   ('Ipsum','Sapien elit in malesuada semper mi, id sollicitudin urna fermentum.', 'consectetur adipiscing elit.', '(104) 804-1341'),
	   ('Sit amet, consectetur.','Ut fusce varius nisl ac ipsum gravida vel pretium tellus','Adipiscing elit fusce vel sapien elit.','(204) 908-4252'),
	   ('Detracto lucilius an usu','Id vocent feugait mei, soluta insolens singulis mel ad.','Eum albucius pericula electram an','(212) 431-9921')


--- Layout
insert into dbo.Layout
values (1,'Big hall' ,'We are a big hall'),
(1,'Small hall', 'We are a big hall'),
(2,'Donec dui ante','Elementum eget purus in, maximus interdum quam'),
(3,'Nulla imperdiet','Nunc sit amet rutrum convallis, tellus urna varius ex'),
(3,'Aliquam eu sagittis orci','Proin eu ex id velit imperdiet ornare quis sed purus'),
(3,'Fusce sed ex maximus','Vestibulum posuere lectus sit amet dolor facilisis'),
(4,'Donec lobortis eget sem id facilisis','Aenean efficitur porttitor risus, vel sagittis urna tristique et'),
(4,'Ut in lacus erat','Suspendisse efficitur vel enim vitae efficitur')


--- Area
insert into dbo.Area
values (1, 'Balcony room', 0, 0),
(1, 'Etiam room', 100, 0),
(2, 'Suspendisse room', 0, 0),
(2, 'Mauris room', 40, 40),
(3, 'Balcony room', 0, 0),
(4, 'Curabitur room', 100, 0),
(4, 'Morbi room', 0, 0),
(4, 'Nunc room', 40, 40),
(5, 'Balcony room', 0, 0),
(6, 'Duis room', 100, 0),
(6, 'Praesent room', 0, 0),
(6, 'Nulla room', 40, 40),
(7, 'Aenean room', 0, 0),
(7, 'Cras room', 100, 0),
(8, 'Quisque room', 0, 0),
(8, 'Maecenas room', 40, 40)

--- Seat
insert into dbo.Seat
values (1, 1, 1),
       (1, 1, 2),
       (1, 2, 1),
	   (1, 2, 2),
	   (1, 3, 1),
	   (2, 1, 1),
       (2, 1, 2),
       (2, 1, 3),
	   (2, 1, 4),
	   (2, 1, 5),
	   (3, 1, 1),
       (3, 1, 2),
       (3, 1, 3),
	   (3, 1, 4),
	   (3, 1, 5),
	   (4, 1, 1),
       (4, 1, 2),
       (4, 1, 3),
	   (4, 1, 4),
	   (4, 1, 5),
	   (5, 1, 1),
       (5, 1, 2),
       (5, 1, 3),
	   (5, 1, 4),
	   (5, 1, 5),
	   (6, 1, 1),
       (6, 1, 2),
       (6, 1, 3),
	   (6, 2, 1),
	   (6, 2, 2),
	   (7, 1, 1),
       (7, 2, 1),
       (7, 3, 1),
	   (7, 4, 1),
	   (7, 5, 1),
	   (8, 1, 1),
       (8, 2, 1),
       (8, 2, 2),
	   (8, 3, 1),
	   (8, 3, 2),
	   (8, 3, 3),
	   (9, 1, 1),
       (9, 1, 2),
       (9, 1, 3),
	   (9, 1, 4),
	   (9, 1, 5),
	   (10, 1, 1),
       (10, 1, 2),
       (10, 1, 3),
	   (10, 1, 4),
	   (10, 1, 5),
	   (10, 2, 1),
       (10, 2, 2),
       (10, 2, 3),
	   (10, 2, 4),
	   (10, 2, 5),
	   (11, 1, 1),
       (11, 1, 2),
       (11, 1, 3),
	   (11, 1, 4),
	   (11, 1, 5),
	   (12, 1, 1),
       (12, 1, 2),
       (12, 1, 3),
	   (12, 1, 4),
	   (12, 1, 5),
	   (13, 1, 1),
       (13, 1, 2),
       (13, 1, 3),
	   (13, 1, 4),
	   (13, 1, 5),
	   (14, 1, 1),
       (14, 1, 2),
       (14, 1, 3),
	   (14, 2, 1),
	   (14, 2, 2),
	   (14, 2, 3),
       (14, 2, 4),
       (14, 2, 5),
	   (14, 2, 6),
	   (15, 1, 1),
       (15, 1, 2),
       (15, 1, 3),
	   (15, 1, 4),
	   (15, 1, 5),
	   (15, 2, 1),
       (15, 2, 2),
       (15, 2, 3),
	   (15, 2, 4),
	   (15, 2, 5),
	   (16, 1, 1),
       (16, 1, 2),
       (16, 1, 3),
	   (16, 1, 4),
	   (16, 1, 5),
	   (16, 2, 1),
       (16, 2, 2),
       (16, 2, 3),
	   (16, 2, 4),
	   (16, 2, 5)

-- Event
insert into dbo.[Event]
values ('Snowman','The film is adaptated by the novel of Nesby', 2, '2018-12-21 10:00:00.0000000 +03:00','2018-12-21 12:00:00.0000000 +03:00','/Content/images/snowman.jpg',1),
	   ('Maecenas','Morbi porttitor, ligula quis commodo vulputate, velit ex pellentesque neque, sit amet consectetur enim dui consectetur felis. Cras pellentesque ligula lorem, in ornare augue lobortis id', 4, '2018-01-19 11:00:00.0000000 +03:00','2018-01-19 16:45:00.0000000 +03:00','/Content/images/poster.jpeg',1),
	   ('Donec finibus ','Donec ullamcorper est id odio imperdiet, a mollis velit interdum. Ut sed dignissim mi, eu tincidunt diam', 8, '2018-11-21 20:05:00.0000000 +03:00','2018-11-21 22:10:00.0000000 +03:00','/Content/images/music.jpg',1),
	   ('Laoreet officiis','Qui in pertinax ullamcorper, solet efficiantur mea cu', 5, '2018-12-04 10:15:00.0000000 +03:00','2018-12-04 12:10:00.0000000 +03:00','/Content/images/avatars.jpg',1)


--- EventArea
insert into dbo.EventArea
values (1, 'First area of first layout', 0, 0,500),
(1, 'Second area of first layout', 100, 0,300),
(2, 'Curabitur room', 100, 0,100),
(2, 'Morbi room', 0, 0,150),
(2, 'Nunc room', 40, 40,300),
(3, 'Quisque room', 0, 0,50),
(3, 'Maecenas room', 40, 40,100),
(4, 'Qui room', 0, 0,50)

--- EventSeat
insert into dbo.EventSeat
values (1, 1, 1, 0),
       (1, 2, 1, 2),
       (1, 1, 2, 0),
	   (1, 1, 3, 0),
	   (1, 2, 2, 0),
	   (1, 2, 3, 0),
       (1, 3, 1, 2),
       (1, 3, 2, 0),
	   (1, 3, 3, 0),
	   (1, 3, 4, 0),
	   (2, 1, 1, 0),
       (2, 1, 2, 2),
       (2, 2, 1, 0),
	   (2, 2, 2, 0),
	   (2, 2, 3, 2),
	   (3, 1, 1, 0),
       (3, 1, 2, 0),
       (3, 1, 3, 0),
	   (3, 1, 4, 0),
	   (3, 1, 5, 2),
	   (4, 1, 1, 0),
       (4, 1, 2, 0),
       (4, 1, 3, 0),
	   (4, 1, 4, 0),
	   (4, 1, 5, 0),
	   (5, 1, 1, 0),
       (5, 2, 1, 0),
       (5, 2, 2, 0),
	   (5, 3, 1, 2),
	   (5, 3, 2, 0),
	   (5, 3, 3, 0),
	   (6, 1, 1, 0),
       (6, 1, 2, 0),
       (6, 1, 3, 0),
	   (6, 1, 4, 0),
	   (6, 1, 5, 0),
	   (6, 2, 1, 0),
       (6, 2, 2, 0),
       (6, 2, 3, 2),
	   (6, 2, 4, 0),
	   (6, 2, 5, 0),
	   (7, 1, 1, 0),
       (7, 1, 2, 0),
       (7, 1, 3, 0),
	   (7, 1, 4, 2),
	   (7, 1, 5, 0),
	   (7, 2, 1, 0),
       (7, 2, 2, 0),
       (7, 2, 3, 2),
	   (7, 2, 4, 0),
	   (7, 2, 5, 2),
	   (8, 1, 1, 0),
	   (8, 1, 2, 0),
	   (8, 1, 3, 0),
	   (8, 1, 4, 0),
	   (8, 1, 5, 0)

--- Roles
insert into dbo.Roles
values ('User'),
       ('EventManager'),
       ('VenueManager')

--- Language
insert into dbo.[Language]
values ('English','en'),
	   ('Russian','ru'),
	   ('Belorussian','be')

--- Users
insert into dbo.Users
values ('17e8e799-2400-4741-8b4a-8b0565effebc','admin', 'admin@admin.com', 'AG2LjFKSPoQf6FRDu/Pg8HTWWAoornnHUKZKMZtZBoCIwvDFFua/PWsoZvQ+dD4bJQ==', 'Ivan','Antonov',2,'Russian Standard Time', 340),
	   ('a8a2ebcf-e1a1-43ef-a27e-ac6cf091e519', 'anyuser', 'anyuser@room.tu', 'AEB8QRizTsaRiBZ3YAKNn8+ho6GSWL/kV+3P4ilqwr2dptbHI7z/itpFySlx71aDBw==', 'Anyuser', 'Petrov', 1, 'Russian Standard Time', 301)


--- UserRole
insert into dbo.UserRole
values ('17e8e799-2400-4741-8b4a-8b0565effebc',1),
	   ('17e8e799-2400-4741-8b4a-8b0565effebc',2),
	   ('17e8e799-2400-4741-8b4a-8b0565effebc',3),
	   ('a8a2ebcf-e1a1-43ef-a27e-ac6cf091e519',1)

--- Purchases
insert into dbo.Purchases
values ('17e8e799-2400-4741-8b4a-8b0565effebc',1,500,'2017-12-01 00:00:00.0000000 +03:00'),
	   ('17e8e799-2400-4741-8b4a-8b0565effebc',2,100,'2017-12-01 00:00:00.0000000 +03:00'),
	   ('17e8e799-2400-4741-8b4a-8b0565effebc',3,50,'2017-10-01 00:00:00.0000000 +03:00'),
	   ('17e8e799-2400-4741-8b4a-8b0565effebc',3,70,'2017-9-10 00:00:00.0000000 +03:00')



