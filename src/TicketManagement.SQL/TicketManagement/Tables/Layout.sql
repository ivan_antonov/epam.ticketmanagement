﻿CREATE TABLE [dbo].[Layout]
(
	[Id] int identity primary key,
	[VenueId] int NOT NULL,
	[Name] nvarchar(90) NOT NULL,
	[Description] nvarchar(120) NOT NULL,
)
