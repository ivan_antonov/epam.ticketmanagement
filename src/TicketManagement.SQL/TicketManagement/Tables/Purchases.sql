﻿CREATE TABLE [dbo].[Purchases]
(
	[Id] INT identity primary key,
    [UserId] NVARCHAR(50) NOT NULL, 
	[EventId] INT NOT NULL,
    [Value] DECIMAL NOT NULL, 
    [Date] DATETIMEOFFSET NOT NULL
)
