﻿CREATE TABLE [dbo].[UserRole]
(
	[Id] INT identity primary key,
    [UserId] NVARCHAR(50) NOT NULL, 
    [RoleId] INT NOT NULL
)
