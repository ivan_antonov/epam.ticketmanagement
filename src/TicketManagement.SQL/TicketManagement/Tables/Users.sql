﻿CREATE TABLE [dbo].[Users]
(
	[Id] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [UserName] NVARCHAR(10) NOT NULL, 
    [Email] NVARCHAR(30) NOT NULL, 
    [PasswordHash] NVARCHAR(MAX) NOT NULL, 
    [Firstname] NVARCHAR(20) NOT NULL, 
    [Surname] NVARCHAR(20) NOT NULL, 
    [LanguageId] INT NOT NULL, 
    [TimeZone] NVARCHAR(50) NOT NULL, 
    [Balance] DECIMAL NOT NULL
)
