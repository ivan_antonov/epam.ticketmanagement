﻿CREATE TABLE [dbo].[Roles]
(
	[Id] INT identity primary key, 
    [Name] NVARCHAR(20) NOT NULL
)
