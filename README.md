# Introductions 
This is a test application for the sale of tickets.
It includes the following projects:

* The application for deploying the database to your server
* The application for providing the necessary functionality with a user-friendly interface that includes the implemented three-tier application architecture. This application is covered by unit tests and integation tests.
* The application for automated testing of the basic functional.


# Getting started

**Before you can take full advantage of the application, you need:**

1. Download repository
1. Open solution `TicketManagement.SQL` from src
1. Configure the connection string to your server in the project properties
1. Rebuild and publish. If you want to work with GUI testing, then it's necessary to mark the point of re-creating the database in the database properties. And deploy one more instance.
1. Import the cetificate from folder Certificates. Use credentials password **admin**
1. Open solution TicketManagement.Common
1. Set `TicketManagement.Web` project as StartUp Project
1. Change the connectionStrings to your connections to the database in file Web.config
1. Change the connectionString in file Global.asax depending on the purpose of running the application
1. Rebuild the solution
1. Set multiple startup projects in solution's propeties (UserApi,Domain,Web)
1. Run projects. For stable work, run vs with administrator priveleges
1. Use credentials for sign in
    * EventManager: **login**: *admin*, **password**: *adminadmin*
    * User: **login**: *anyuser*, **password**: *useruser*

#### If you have break point in wcf exception, turn off break point for throw faultservice `1

#### If you want to run integration tests

1. Choose `Tests/TicketManagement.IntegrationTests` project
1. Change the connectionStrings "test" to your connection to the database in file app.config
1. Change the FILENAME to your path in file Scripts/SnapshotScripts.sql if it is necessay
1. Install NUnit 3 Test Adapter from Extensions and Updates
1. Run all tests from TestExplorer

#### If you want to run  GUI testing

* Open solution `TicketManagement.AQA` from src
* Configure tests in file app.config

```html
    <add key="Browser" value="chrome" />
    <add key="PageLoad" value="50000" />
    <add key="ImplicitWait" value="15000" />
    <add key="AsynchronousJavaScript" value="15000" />
    <add key="ApplicationUrl" value="http://localhost:62201" />
```

* Rebuild project and run tests from unit test explorer
